/*
 * pelicula.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */
#include <string>
#include <iostream>
#include <list>
#include "../datatype/header/dtFuncion.h"
#include "../datatype/header/dtFecha.h"
#include "../datatype/header/dtHora.h"
#include "../header/Reserva.h"
using namespace std;

#ifndef FUNCION_H_
#define FUNCION_H_


class Funcion{

private:
	int nroFuncion;
	dtFecha fecha;
	dtHora hora;
	list<Reserva*> reservas;

public:

	Funcion();
	Funcion(int nroFuncion, dtFecha fecha, dtHora hora);
	virtual ~Funcion();

	//gettes
	int getNro();
	dtFecha getFecha();
	dtHora getHora();
	list<Reserva*> getReserva();

	//setters
	void setNro(int nro);
	void setFecha(dtFecha fecha);
	void setHora(dtHora hora);
	void setReserva(list<Reserva*> reserva);


	void agregarReserva(Reserva* reserva);

	list<dtFuncion> obtenerFuncion();
};




#endif /* FUNCION_H_ */
