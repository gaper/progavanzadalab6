#ifndef HEADER_ICONTROLADORSESION_H_
#define HEADER_ICONTROLADORSESION_H_

//#include "../datatype/header/dtPelicula.h" //DtUsuario.h

#include <list>
#include <string>
using namespace std;

class IControladorSesion{
public:
	virtual void ingresarNick (string nick) = 0;
	virtual void ingresarPass (string pass) = 0;
	virtual void cerrarSesion()=0;

	virtual bool sesionIniciada()=0;
	virtual string getNickSesion()=0;
	virtual bool existeNick(string nick)=0;
	virtual bool passCorrecta(string nick,string pass)=0;
	virtual void iniciarSesion(string nick)=0;

	virtual bool sesionEsAdmin(string nick)=0;

	virtual ~IControladorSesion(){};

};

#endif
