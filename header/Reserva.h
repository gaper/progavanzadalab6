/*
 * Reserva.h
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */
#include <string>
#include <iostream>
#include <list>

#ifndef RESERVA_H_
#define RESERVA_H_

using namespace std;

class Reserva {

private:
	int cantAsientos;
	float costo;
	string nick;

public:
	Reserva();
	Reserva(int cantAsientos, float costo);
	virtual ~Reserva();

	//getters
	int getCantAsientos();
	float getCosto();
	string getNick();

	//setters
	void setCantAsientos(int cantAsientos);
	void setCosto(float costo);
	void setNick(string nick);
};

#endif /* RESERVA_H_ */
