#ifndef HEADER_PUNTAJE_H_
#define HEADER_PUNTAJE_H_

#include "Usuario.h"

#include <string>
#include <iostream>
#include <list>
using namespace std;

class Puntaje {
	private:
		int puntaje;
		Usuario* usuario;
	public:
		Puntaje();
		Puntaje(int puntaje, Usuario* usuario);
		~Puntaje();

		int getPuntaje();
		void setPuntaje(int puntaje);

		bool estaPuntuada();
		bool estaPuntuadaPor(string nick);

};

#endif /* HEADER_PUNTAJE_H_ */
