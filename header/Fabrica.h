#ifndef FABRICA_H_
#define FABRICA_H_

#include "../header/IControladorPelicula.h"
#include "../header/IControladorSesion.h"
#include "../header/IControladorReloj.h"

class Fabrica {

private:
	static Fabrica * instance;
	Fabrica();

public:
	static Fabrica* getInstancia();
	IControladorSesion* getIControladorSesion();
	IControladorReloj* getIControladorReloj();
	IControladorPelicula* getIControladorPelicula();
	~Fabrica();

};


#endif /* FABRICA_H_ */
