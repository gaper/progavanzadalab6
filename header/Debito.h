/*
 * Debito.h
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */
#include <string>
#include "../header/Reserva.h"
using namespace std;

#ifndef DEBITO_H_
#define DEBITO_H_

class Debito : public Reserva{

private:
	string banco;

public:
	Debito();
	Debito(string banco);
	virtual ~Debito();

	//getters
	string getBanco();

	//setters
	void setBanco(string banco);

};

#endif /* DEBITO_H_ */
