#ifndef HEADER_RELOJ_H_
#define HEADER_RELOJ_H_

class Reloj{
    private:
		int dia;
		int mes;
		int anio;
		int hora;
		int minutos;

		Reloj();
        ~Reloj();
        static Reloj* instance;
    public:
        static Reloj* getInstancia();

        void setDia(int);
		void setMes(int);
		void setAnio(int);
		void setHora(int);
		void setMinutos(int);

		int getDia();
		int getMes();
		int getAnio();
		int getHora();
		int getMinutos();

};



#endif /* HEADER_RELOJ_H_ */
