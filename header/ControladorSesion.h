#ifndef HEADER_CONTROLADORSESION_H_
#define HEADER_CONTROLADORSESION_H_

#include "../header/IControladorSesion.h"
#include "../header/Sesion.h"
#include "../header/Usuario.h"
//#include "../header/DtUsuario.h"
#include "../header/ManejadorUsuario.h"

#include <string>
using namespace std;

class ControladorSesion : public IControladorSesion  {

private:

	string nickRecordado;

	static ControladorSesion* instancia;

	ControladorSesion();

public:

	static ControladorSesion* getInstancia();

	void ingresarNick (string nick);
	void ingresarPass (string pass);
    void cerrarSesion();

    bool sesionIniciada();
    string getNickSesion();
    bool existeNick(string nick);
    bool passCorrecta(string nick,string pass);
    void iniciarSesion(string nick);

    bool sesionEsAdmin(string nick);

	 virtual ~ControladorSesion();
};

#endif /* HEADER_CONTROLADORSESION_H_ */
