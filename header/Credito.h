/*
 * Credito.h
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */
#include <string>
#include "../header/Reserva.h"
using namespace std;

#ifndef CREDITO_H_
#define CREDITO_H_

class Credito : public Reserva{

private:
	string financiera;
	float descuento;

public:
	Credito();
	Credito(string financiera, float descuento);
	virtual ~Credito();

	//getters
	string getFinanciera();
	float getDescuento();

	//setters
	void setFinanciera(string financiera);
	void setDescuento(float descuento);

};

#endif /* CREDITO_H_ */
