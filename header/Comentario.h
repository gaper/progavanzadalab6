#ifndef HEADER_COMENTARIO_H_
#define HEADER_COMENTARIO_H_

#include "Usuario.h"

#include <string>
#include <iostream>
#include <list>
using namespace std;

class Comentario {
	private:
		string comentario;
		Usuario* usuario;
	public:
		Comentario();
		Comentario(string comentario, Usuario* usuario);
		~Comentario();

		string getComentario();
		void setComentario(string comentario);

		bool estaComentada();
		bool estaComentadaPor(string nick);

};


#endif /* HEADER_COMENTARIO_H_ */
