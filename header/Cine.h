/*
 * cine.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */
#include <string>
#include <iostream>
#include <list>
#include "../datatype/header/dtFuncion.h"
#include "../datatype/header/dtDireccion.h"
#include "../datatype/header/dtSala.h"
#include "Sala.h"
using namespace std;

#ifndef CINE_H_
#define CINE_H_



class Cine{

private:

	int nroCine;

	dtDireccion direccion;

	list<Sala*> salas;

public:

	Cine();

	Cine(int nroCine, dtDireccion direccion);

	virtual ~Cine();

	//getters
	int getNroCine();

	list<Sala*> getSalas();

	dtDireccion getDireccion();


	//setters
	void setNroCine(int nroCine);

	void setDireccion(dtDireccion direccion);

	void setSalas(list<Sala*> salas);

	void agregarSala(Sala* s);



	list<dtSala> obtenerFuncion();


};




#endif /* CINE_H_ */
