/*
 * ControladorPelicula.h
 *
 *  Created on: Jun 17, 2019
 *      Author: gaston
 */

#ifndef HEADER_CONTROLADORPELICULA_H_
#define HEADER_CONTROLADORPELICULA_H_

#include "IControladorPelicula.h"
#include "Cine.h"
#include "Pelicula.h"
#include "Financiera.h"
#include "Credito.h"
#include "Debito.h"

class ControladorPelicula : public IControladorPelicula  {

private:
			list<Cine*> cines;

			static ControladorPelicula* instancia;

			ControladorPelicula();

			list<Pelicula*> peliculas;

			list<Financiera*> financieras;

			float costo=150;

public:

	list<Pelicula*>  getPeliculas();

    static ControladorPelicula* getInstancia();

    void altaCine(dtCine cine);

    void altaFuncion(string pelicula, int cine, int sala,dtFuncion fun);

	dtPelicula ingresarPelicula (string titulo);

	list<string> listarPeliculas();

	dtPelicula selPelicula(string titulo) ;

	list<dtSala*> listarSalas(int cine);

	list<dtCine*> listarSalasPeliculas(string pelicula);

	list<int> verInfoAd(string titulo);

	void eliminarPelicula();

    list<string> listarCines();

	list<dtSala> ingresarCine(int nroCine,string pelicula);

	void selFuncion(string idFuncion, int cantAsiento);

	void ingMetPago(string metodo);

	void ingBanco(string nomB);

	float ingFinanciera(string nomF);

	 float verPrecio();

	 void confirmar();

	 void listaReservasUsuario(string nick);

	 void crearPeliculas();

	 bool estaPuntuada(string pel, string nick);
	 int getPuntaje(string pel, string nick);
	 void setPuntaje(string pel, string nick, int puntaje);
	 bool existePelicula(string pel);

	 bool estaComentada(string pel, string nick);
	 string getComentario(string pel, string nick);
	 void setComentario(string pel, string nick, string comentario);

	 void ingBanco(int idFuncion, int cantAsiento,string nomB,string titulo);
	 float verPrecio(int cantAsientos);
	 float verPrecioC(string financiera, int cantAsientos);
	 void confirmarD(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine);
	 void confirmarC(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine);
	 bool valFinanciera(string financiera);
	 void crearFinancieras();

	 virtual ~ControladorPelicula();
};


#endif /* HEADER_CONTROLADORPELICULA_H_ */
