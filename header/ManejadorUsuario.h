#ifndef HEADER_MANEJADORUSUARIO_H_
#define HEADER_MANEJADORUSUARIO_H_

#include <set>
using namespace std;
#include <iostream>

#include "../header/Usuario.h"
#include "../datatype/header/dtUsuario.h"

class ManejadorUsuario{
        private:
                static ManejadorUsuario* instancia;
                ManejadorUsuario();
                set<Usuario*> listaUsuarios;
        public:
                static ManejadorUsuario* getInstancia();
                virtual ~ManejadorUsuario();


				void eliminarUsuario(string);
                Usuario* buscarUsuario(string);
                bool existeUsuario(string);

                void agregarUsuario(dtUsuario);

};

#endif /* HEADER_MANEJADORUSUARIO_H_ */
