/*
 * sala.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */
#include <string>
#include <iostream>
#include <list>
#include "../datatype/header/dtFuncion.h"
#include "../datatype/header/dtFecha.h"
#include "../datatype/header/dtHora.h"
#include "../header/Funcion.h"
using namespace std;

#ifndef SALA_H_
#define SALA_H_

class Sala{

private:
	int nroSala;
	int capacidad;
	list<Funcion*> funciones;

public:

	Sala();
	Sala(int nroSala, int capacidad);
	virtual ~Sala();

	//gettes
	int getNro();
	int getCapacidad();
	list<Funcion*> getFuncion();

	//setters
	void setNro(int nro);
	void setCapacidad(int capacidad);
	void setFuncion(list<Funcion*> funcion);
	void agregarFuncion(Funcion* f);

	list<dtFuncion*>* obtenerFuncion();


};




#endif /* SALA_H_ */
