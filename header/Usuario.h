#ifndef USUARIO_H_
#define USUARIO_H_

#include <string>
#include <iostream>
#include <list>
using namespace std;

class Usuario {

protected:
	string nick;
	string pass;
	string imagen;
	bool esAdmin;

public:
	Usuario();
	Usuario(string nick, string pass, string imagen, bool esAdmin);
	virtual ~Usuario();

	//getters
	string getNick();
	string getPass();
	string getImagen();
	bool getEsAdmin();

	//setters
	void setNick(string nick);
	void setPass(string pass);
	void setImagen(string imagen);
	void setEsAdmin(bool esAdmin);
};


#endif /* USUARIO_H_ */
