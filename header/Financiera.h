/*
 * Financiera.h
 *
 *  Created on: 25 jun 2019
 *      Author: Usuario
 */

#include <string>
using namespace std;

#ifndef HEADER_FINANCIERA_H_
#define HEADER_FINANCIERA_H_

class Financiera{
private:
	string nombre;
	float descuento;
public:
	Financiera();
	Financiera(string nombre, float descuento);
	string getNombre();
	float getDescuento();

};



#endif /* HEADER_FINANCIERA_H_ */
