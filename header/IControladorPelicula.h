#ifndef HEADER_ICONTROLADORPELICULA_H_
#define HEADER_ICONTROLADORPELICULA_H_

#include "../datatype/header/dtPelicula.h"
#include "../datatype/header/dtFuncion.h"

#include <string>
#include <list>
#include "../datatype/header/dtCine.h"
using namespace std;

class IControladorPelicula{

public:

	virtual void altaCine (dtCine cine) = 0;

	virtual void altaFuncion(string pelicula, int cine, int sala,dtFuncion fun) = 0;

	virtual dtPelicula ingresarPelicula (string titulo) = 0;

	virtual list<string> listarPeliculas() = 0;

	virtual dtPelicula selPelicula(string titulo) = 0;

	virtual list<dtSala*> listarSalas(int cine) = 0;

	virtual list<dtCine*> listarSalasPeliculas(string pelicula) = 0;

	virtual list<int> verInfoAd(string titulo) = 0;

	virtual void eliminarPelicula() = 0;

	virtual list<string> listarCines() = 0;

	virtual list<dtSala> ingresarCine(int nroCine,string pelicula) = 0;

	virtual void selFuncion(string idFuncion, int cantAsiento) = 0;

	virtual void ingMetPago(string metodo) = 0;

	virtual void ingBanco(string nomB) = 0;

	virtual float ingFinanciera(string nomF) = 0;

	virtual float verPrecio() = 0;

	virtual void confirmar() = 0;

	virtual void crearPeliculas() = 0;

	virtual void listaReservasUsuario(string nick) = 0;

	virtual bool estaPuntuada(string pel, string nick)=0;
	virtual int getPuntaje(string pel, string nick)=0;
	virtual void setPuntaje(string pel, string nick, int puntaje)=0;
	virtual bool existePelicula(string pel)=0;

	virtual bool estaComentada(string pel, string nick)=0;
	virtual string getComentario(string pel, string nick)=0;
	virtual void setComentario(string pel, string nick, string comentario)=0;

	 virtual void ingBanco(int idFuncion, int cantAsiento,string nomB,string titulo) = 0;
	 virtual float verPrecio(int cantAsientos)=0;
	 virtual float verPrecioC(string financiera, int cantAsientos)=0;
	 virtual void confirmarD(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine)=0;
	 virtual void confirmarC(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine)=0;
	 virtual bool valFinanciera(string financiera)=0;
	 virtual void crearFinancieras() = 0;

	virtual ~IControladorPelicula(){};

};


#endif /* HEADER_ICONTROLADORPELICULA_H_ */
