/*
 * pelicula.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */

#ifndef PELICULA_H_
#define PELICULA_H_

#include <string>
#include <iostream>
#include <list>
#include "../datatype/header/dtPelicula.h"
#include "../datatype/header/dtFuncion.h"
#include "../header/Funcion.h"
#include "../header/Sala.h"
#include "../header/Cine.h"
#include "../header/Puntaje.h"
#include "../header/Comentario.h"
#include "../header/ManejadorUsuario.h"

using namespace std;




class Pelicula{

private:

	string titulo, poster, sinopsis;

	float puntajePromedio;

	//list<Funcion*> funciones;

	list<Cine*> cines;

	list<Puntaje*> puntajes;

	list<Comentario*> comentarios;

public:

	Pelicula();

	Pelicula(string titulo, string poster, string sinopsis, float puntajePromedio);

	virtual ~Pelicula();

	//gettes
	string getTitulo();

	string getPoster();

	string getSinopsis();

	float getPromedio();

	float getPuntajePromedio();

	list<Cine*> getCines();

	list<Funcion*> getFunciones();

	list<Puntaje*> getPuntajes();

	list<Comentario*> getComentarios();

	//setters

	void setPuntajePromedio(float puntajePromedio);

	void setSinopsis(string sinopsis);

	void setTitulo(string titulo);

	void setCines(list<Cine*> cines);

	void setFunciones(list<Funcion*> funciones);

	void setPuntajes(list<Puntaje*> puntajes);

	void setComentarios(list<Comentario*> comentarios);

	void setPoster(string poster);



	bool estaPuntuada(string nick);

	int getPuntaje(string nick);

	void setPuntaje(string nick, int puntaje);

	bool estaComentada(string nick);

	string getComentario(string nick);

	void setComentario(string nick, string comentario);

	void agregarCine(Cine* c);

	dtPelicula selPelicula();

	list<int> verInfoAd();

	list<dtSala> ingresarCine(int nroCine);

	void agregarReserva(Reserva* reserva);


};
#endif /* PELICULA_H_ */
