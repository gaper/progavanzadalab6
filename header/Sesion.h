#ifndef HEADER_SESION_H_
#define HEADER_SESION_H_

#include <string>
using namespace std;

class Sesion{
    private:
        string nick;
        Sesion();
        ~Sesion();
        static Sesion* instance;
    public:
        static Sesion* getInstancia();
        //Setters
        void setNick(string);
        //Getters
        string getNick();
};

#endif /* HEADER_SESION_H_ */
