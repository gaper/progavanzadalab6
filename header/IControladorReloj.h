
#ifndef HEADER_ICONTROLADORRELOJ_H_
#define HEADER_ICONTROLADORRELOJ_H_

#include "../datatype/header/dtReloj.h"
#include <string>
#include <list>
using namespace std;

class IControladorReloj{

public:

	virtual void modificarReloj (dtReloj reloj) = 0;

	virtual dtReloj consultarReloj () = 0;

	virtual ~IControladorReloj(){};
};


#endif /* HEADER_ICONTROLADORRELOJ_H_ */
