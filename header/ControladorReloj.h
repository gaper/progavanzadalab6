#ifndef HEADER_CONTROLADORRELOJ_H_
#define HEADER_CONTROLADORRELOJ_H_

#include "../header/IControladorReloj.h"
#include "../datatype/header/dtReloj.h"
#include "../header/Reloj.h"

class ControladorReloj : public IControladorReloj  {

private:
		static ControladorReloj* instancia;
		ControladorReloj();
public:

	static ControladorReloj* getInstancia();

	void modificarReloj (dtReloj reloj);

	dtReloj consultarReloj();

	virtual ~ControladorReloj();
};


#endif /* HEADER_CONTROLADORRELOJ_H_ */
