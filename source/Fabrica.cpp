
#include "../header/Fabrica.h"
#include "../header/ControladorPelicula.h"
#include "../header/ControladorSesion.h"
#include "../header/ControladorReloj.h"

Fabrica* Fabrica::instance = NULL;

Fabrica::Fabrica() {}

Fabrica* Fabrica::getInstancia(){
    if (instance == NULL)
    	instance = new Fabrica();
    return instance;
}

IControladorSesion* Fabrica::getIControladorSesion(){
    IControladorSesion* cs = ControladorSesion::getInstancia();
    return cs;
}

IControladorReloj* Fabrica::getIControladorReloj(){
    IControladorReloj* cr = ControladorReloj::getInstancia();
    return cr;
}

IControladorPelicula* Fabrica::getIControladorPelicula(){
    IControladorPelicula* cp = ControladorPelicula::getInstancia();
    return cp;
}


Fabrica::~Fabrica() {}
