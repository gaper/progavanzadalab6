#include "../header/Comentario.h"

Comentario::Comentario(){
	this->comentario="";
	this->usuario=NULL;
}

Comentario::Comentario(string comentario, Usuario* usuario){
	this->comentario=comentario;
	this->usuario=usuario;
}
Comentario::~Comentario(){}

string Comentario::getComentario(){
	return this->comentario;
}
void Comentario::setComentario(string Comentario){
	this->comentario=comentario;
}

bool Comentario::estaComentada(){
	if(this->usuario==NULL){
		return false;
	}else{
		return true;
	}
}

bool Comentario::estaComentadaPor(string nick){
	if(nick.compare(this->usuario->getNick()) == 0){
		return true;
	}else{
		return false;
	}
}
