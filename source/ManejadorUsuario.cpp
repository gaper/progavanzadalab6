#include "../header/ManejadorUsuario.h"
#include <iostream>
#include <string>
using namespace std;

ManejadorUsuario* ManejadorUsuario::instancia = 0;

ManejadorUsuario* ManejadorUsuario:: getInstancia(){
	if (instancia == NULL){
		instancia = new ManejadorUsuario();
	}
	return instancia;
}

ManejadorUsuario:: ManejadorUsuario(){
	Usuario* u = new Usuario("g", "g","img", false);
	listaUsuarios.insert(u);
	Usuario* u1 = new Usuario("p", "p","img", true);
	listaUsuarios.insert(u1);
	Usuario* u2 = new Usuario("h", "h","img", false);
	listaUsuarios.insert(u2);
}

ManejadorUsuario::~ManejadorUsuario(){
	set<Usuario*>::iterator i;
	for(i = listaUsuarios.begin(); i!= listaUsuarios.end(); ++i){
		delete *i;
	}
}

void ManejadorUsuario:: eliminarUsuario(string nick){
	for (set<Usuario*>::iterator it=listaUsuarios.begin(); it!=listaUsuarios.end(); ++it){
		if ((*it)->getNick() == nick){
			listaUsuarios.erase((*it));
		}
	}
}

Usuario* ManejadorUsuario:: buscarUsuario(string nick){
	for (set<Usuario*>::iterator it=listaUsuarios.begin(); it!=listaUsuarios.end(); ++it){
		if (nick.compare((*it)->getNick()) == 0){
			return (*it);
		}
	}
	return NULL;
}

bool ManejadorUsuario:: existeUsuario(string nick){
	for (set<Usuario*>::iterator it=listaUsuarios.begin(); it!=listaUsuarios.end(); ++it){
		if (nick.compare((*it)->getNick()) == 0){
			return true;
		}
	}
	return false;
}

void ManejadorUsuario:: agregarUsuario(dtUsuario usuario){
	Usuario* u = new Usuario(usuario.getNick(), usuario.getPass(), usuario.getImagen(), usuario.getEsAdmin());
	listaUsuarios.insert(u);
}
