/*
 * Credito.cpp
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */

#include "../header/Credito.h"

Credito::Credito() {

}
Credito::Credito(string financiera, float descuento){
	this->financiera = financiera;
	this->descuento = descuento;
}

//getters
string Credito::getFinanciera(){
	return this->financiera;
}
float Credito::getDescuento(){
	return this->descuento;
}

//setters
void Credito::setFinanciera(string financiera){
	this->financiera = financiera;
}
void Credito::setDescuento(float descuento){
	this->descuento = descuento;
}
Credito::~Credito() {

}

