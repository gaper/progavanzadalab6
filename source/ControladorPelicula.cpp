#include "../header/ControladorPelicula.h"


ControladorPelicula* ControladorPelicula::instancia = NULL;

ControladorPelicula::ControladorPelicula(){}

list<Pelicula*> ControladorPelicula::getPeliculas(){
		return this->peliculas;
	}

ControladorPelicula* ControladorPelicula::getInstancia(){

	if (instancia == NULL)
	instancia = new ControladorPelicula();
	return instancia;
}

void ControladorPelicula::altaCine(dtCine cine){

	Cine* c = new Cine(cine.getNro(), cine.getDireccion());

	list<dtSala*> salas = cine.getSalas();

	list<dtSala*>::iterator it = salas.begin();

	list<Sala*> gets = c->getSalas();

	dtSala* s;

	while (it != salas.end()) {
		s = *it;
		cout << s->getNroSala() << endl;
		Sala* ss = new Sala(s->getNroSala(), s->getCapacidad());

		gets.push_back(ss);

		it++;
	}
	c->setSalas(gets);
	this->cines.push_back(c);


}

void ControladorPelicula::altaFuncion(string pelicula, int cine, int sala,dtFuncion fun){

	Cine* bc = NULL;

	Cine* selec = NULL;

	list<Cine*>::iterator itc = cines.begin();

//Agrego la funcion al cine

	while (itc != cines.end()) {

		bc = *itc;

		if (cine == bc->getNroCine()) {

			selec = *itc;
		}

		itc++;
	}

	Sala* ss = NULL;

	Sala* selss = NULL;

	list<Sala*> salabb = selec->getSalas();

	list<Sala*>::iterator its = salabb.begin();

	while (its != salabb.end()) {
		ss = *its;
		if (sala == ss->getNro()) {

			selss = *its;
		}

		its++;
	}

	Funcion* funcc = new Funcion(fun.getNro(), fun.getFecha(), fun.getHora());
	selss->agregarFuncion(funcc);

// termine de agregar funcion en cine

// Agrego la función a la película

	Pelicula* p = NULL;

	Pelicula* selp = NULL;

	list<Pelicula*>::iterator it = peliculas.begin();

	while (it != peliculas.end()) {

		p = *it;

		if (pelicula.compare(p->getTitulo()) == 0) {

			selp = *it;
		}

		it++;
	}

	if (selp != NULL) {

		Cine* c = NULL;

		Cine* selc = NULL;

		list<Cine*> cin = selp->getCines();

		list<Cine*>::iterator it = cin.begin();

		while (it != cin.end()) {

			c = *it;

			if (cine == c->getNroCine()) {

				selc = *it;
			}

			it++;
		}

		if (selc != NULL) {

			Sala* s = NULL;

			Sala* sels = NULL;

			list<Sala*> salab = selc->getSalas();

			list<Sala*>::iterator it = salab.begin();

			while (it != salab.end()) {
				s = *it;
				if (sala == s->getNro()) {

					sels = *it;
				}

				it++;
			}

			if (sels != NULL) {

				Funcion* func = new Funcion(fun.getNro(), fun.getFecha(),
						fun.getHora());
				sels->agregarFuncion(func);

			} else {

				Sala* nuevas = new Sala(sala, 0);
				Funcion* nuevaf = new Funcion(fun.getNro(), fun.getFecha(),
						fun.getHora());
				nuevas->agregarFuncion(nuevaf);
				selc->agregarSala(nuevas);

			}

		} else {

			dtDireccion d = dtDireccion("calle", 111);
			Cine* nuevoc = new Cine(cine, d);
			Sala* nuevas = new Sala(sala, 0);
			Funcion* nuevaf = new Funcion(fun.getNro(), fun.getFecha(),
					fun.getHora());

			nuevas->agregarFuncion(nuevaf);
			nuevoc->agregarSala(nuevas);
			selp->agregarCine(nuevoc);

		}

	} else {

		cout << "No existe la pelicula ingresada: " << pelicula << endl;

	}


}


dtPelicula ControladorPelicula::ingresarPelicula (string titulo){}

list<string> ControladorPelicula::listarPeliculas(){

	list<string> lista;
	string tit;
	//list<Pelicula*> pel;
	Pelicula* p;

	//pel=getPeliculas();

	list<Pelicula*>::iterator it= peliculas.begin();

	while(it != peliculas.end()){
		    	p = *it;
		    	tit = p->getTitulo();// dynamic_cast<gato*>(*it);
		    	lista.push_back(tit);
		    	it++;
	}
	return lista;
}

dtPelicula ControladorPelicula::selPelicula(string titulo){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	dtPelicula pel;
	while(it != peliculas.end()){
			    	p = *it;
			    	if(p->getTitulo().compare(titulo)==0){
			    		pel=p->selPelicula();
			    		return pel;
			    	}
			    	it++;
	}
}


list<int> ControladorPelicula::verInfoAd(string titulo){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	list<int> cines;
	while(it != peliculas.end()){
			    	p = *it;
			    	if(p->getTitulo().compare(titulo)==0){
			    		cines=p->verInfoAd();
			    		return cines;
			    		}
			    	it++;
			    	}

}

list<dtSala> ControladorPelicula::ingresarCine(int nroCine, string pelicula){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	list<dtSala> funcion;
	while(it != peliculas.end()){
					p = *it;
					if(p->getTitulo().compare(pelicula)==0){
						funcion=p->ingresarCine(nroCine);
						return funcion;
						}
					it++;
					}

}


list<dtSala*> ControladorPelicula::listarSalas(int cine){

	Cine* c = NULL;
	Cine* selc = NULL;

	list<dtSala*> sal = list<dtSala*>();

	list<Cine*>::iterator it=  cines.begin();

	while(it != cines.end()){

						c = *it;

						if(c->getNroCine() == cine){

							selc = *it;
						}

						it++;
					}


		if(selc != NULL){


			list<Sala*> s = selc->getSalas();

			list<Sala*>::iterator it=  s.begin();

			Sala* sw;

				while(it != s.end()){

									sw = *it;

									list<dtFuncion*> fun = list<dtFuncion*>();

									list<Funcion*> f = sw->getFuncion();

									list<Funcion*>::iterator itb=  f.begin();

									Funcion* func;

									while(itb != f.end()){

											func = *itb;

											dtFuncion* dtf = new dtFuncion(func->getNro(),func->getFecha(),func->getHora());

											fun.push_back(dtf);

											itb++;
									}

									dtSala* ss = new dtSala(sw->getNro(),sw->getCapacidad(),fun);

									sal.push_back(ss);

									it++;
								}


		}


		return sal;

}

list<dtCine*> ControladorPelicula::listarSalasPeliculas(string pelicula) {

	list<dtCine*> cine = list<dtCine*>();

	list<dtSala*> sal;

	list<Pelicula*>::iterator it = peliculas.begin();

	Pelicula* p;

	while (it != peliculas.end()) {

		p = *it;

		if (pelicula.compare(p->getTitulo()) == 0) {

			list<Cine*> c = p->getCines();

			Cine* selc = NULL;

			sal = list<dtSala*>();

			list<Cine*>::iterator itc = c.begin();

			while (itc != c.end()) {

				selc = *itc;

				list<Sala*> s = selc->getSalas();

				list<Sala*>::iterator its = s.begin();

				Sala* sw;

				while (its != s.end()) {

					sw = *its;

					list<dtFuncion*> fun = list<dtFuncion*>();

					list<Funcion*> f = sw->getFuncion();

					list<Funcion*>::iterator itb = f.begin();

					Funcion* func;

					while (itb != f.end()) {

						func = *itb;

						dtFuncion* dtf = new dtFuncion(func->getNro(),
								func->getFecha(), func->getHora());

						fun.push_back(dtf);

						itb++;
					}

					dtSala* ss = new dtSala(sw->getNro(), sw->getCapacidad(),
							fun);

					sal.push_back(ss);

					its++;
				}

				dtCine* cc = new dtCine(selc->getNroCine(),
						selc->getDireccion(), sal);
				cine.push_back(cc);

				itc++;
			}

		}

		it++;
	}

	return cine;

}


void ControladorPelicula::eliminarPelicula(){}

list<string> ControladorPelicula::listarCines(){

	list<string> lista;
	string cine;
	Cine* c;

	list<Cine*>::iterator it= cines.begin();

	while(it != cines.end()){
		    	c = *it;
		    	dtDireccion d = c->getDireccion();

		    	cine = to_string(c->getNroCine()) + "-"+ d.getCalle()+ " " + to_string(d.getNro());
		    	lista.push_back(cine);
		    	it++;
	}
	return lista;

}


void ControladorPelicula::selFuncion(string idFuncion, int cantAsiento){}

void ControladorPelicula::ingMetPago(string metodo){}

void ControladorPelicula::ingBanco(string nomB){}

float ControladorPelicula::ingFinanciera(string nomF){}

float ControladorPelicula::verPrecio(){}

void ControladorPelicula::confirmar(){}

bool ControladorPelicula::estaPuntuada(string pel, string nick){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			return p->estaPuntuada(nick);
		}
		it++;
	}
	return false;
}

int ControladorPelicula::getPuntaje(string pel, string nick){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			return p->getPuntaje(nick);
		}
		it++;
	}
	return 0;
}

void ControladorPelicula::setPuntaje(string pel, string nick, int puntaje){

	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			p->setPuntaje(nick, puntaje);
			it = peliculas.end();
			it--;
		}
		it++;
	}
}

bool ControladorPelicula::existePelicula(string pel){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			return true;
		}
		it++;
	}
	return false;
}


void ControladorPelicula::ingBanco(int idFuncion, int cantAsiento,string nomB,string titulo){

	Reserva* reserva=new Reserva(cantAsiento,cantAsiento*this->costo);
	Debito* deb=dynamic_cast<Debito*>(reserva);
	deb->setBanco(nomB);


	//Falta agregar deb a coleccion de reservas del usuario y de la funcion(?
}


float ControladorPelicula::verPrecio(int cantAsientos){
	float precio= cantAsientos*this->costo;
	return precio;
}

float ControladorPelicula::verPrecioC(string financiera, int cantAsientos){
	float precio;
	list<Financiera*>::iterator it= this->financieras.begin();
	Financiera* f;
	while(it != this->financieras.end()){
					f = *it;
					if(f->getNombre().compare(financiera)==0){
						precio=cantAsientos * this->costo;
						float Desc = precio * f->getDescuento();
						float total = precio - Desc;
						return total;
					}
					it++;
	}
}

void ControladorPelicula::confirmarD(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine){

	//Reserva* reserva=new Reserva(cantAsiento,cantAsiento*this->costo);
	//Debito* deb=dynamic_cast<Debito*>(reserva);
	//deb->setBanco(nomB);

	Debito* deb = new Debito(nomB);
	int cost = cantAsiento*this->costo;
	deb->setCantAsientos(cantAsiento);
	deb->setNick(nick);
	deb->setCosto(cost);

	/*list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	while(it != peliculas.end()){
					p = *it;
					if(p->getTitulo().compare(titulo)==0){
						//p->agregarReserva(reserva);
						//FALTA AGREGAR RESERVA A USUARIO
						return;
					}
					it++;
					}*/

	Pelicula* p = NULL;

		Pelicula* selp = NULL;

		list<Pelicula*>::iterator it = peliculas.begin();

		while (it != peliculas.end()) {

			p = *it;

			if (titulo.compare(p->getTitulo()) == 0) {

				selp = *it;
			}

			it++;
		}

		if (selp != NULL) {

			Cine* c = NULL;

			Cine* selc = NULL;

			list<Cine*> cin = selp->getCines();

			list<Cine*>::iterator it = cin.begin();

			while (it != cin.end()) {

				c = *it;

				if (cine == c->getNroCine()) {

					selc = *it;
				}

				it++;
			}

			if (selc != NULL) {

				Sala* s = NULL;

				Sala* sels = NULL;

				list<Sala*> salab = selc->getSalas();

				list<Sala*>::iterator it = salab.begin();

				while (it != salab.end()) {
					s = *it;
					if (sala == s->getNro()) {

						sels = *it;

					}

					it++;
				}

				Funcion* f = NULL;

				Funcion* self = NULL;

				list<Funcion*> funcionb = sels->getFuncion();

				list<Funcion*>::iterator itf = funcionb.begin();

				while (itf != funcionb.end()) {
								f = *itf;
								if (idFuncion == f->getNro()) {

									self = *itf;

								}

								itf++;
							}

					self->agregarReserva(deb);
			}

		} else {

			cout << "No existe la pelicula ingresada: " << titulo << endl;

		}
}

void ControladorPelicula::confirmarC(int idFuncion, int cantAsiento, string nomB, string titulo, string nick, int sala, int cine){


	//Reserva* reserva=new Reserva(cantAsiento,0);
	//Credito* cred=dynamic_cast<Credito*>(reserva);
	Credito* cred= new Credito(nomB,0);

	Financiera* fina;

	list<Financiera*>::iterator itfi = financieras.begin();

	while (itfi != financieras.end()) {

			fina = *itfi;

			if (nomB.compare(fina->getNombre()) == 0) {

				cred->setDescuento(fina->getDescuento());
			}

			itfi++;
		}
		cred->setCantAsientos(cantAsiento);
		float cost = cantAsiento * this->costo;
		float descCred = cred->getDescuento();
		float Desc = cost * descCred;
		float total = cost - Desc;
		cred->setCosto(total);
		cred->setNick(nick);

	/*list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	while(it != peliculas.end()){
					p = *it;
					if(p->getTitulo().compare(titulo)==0){
						//p->agregarReserva(reserva);
						//FALTA AGREGAR RESERVA A USUARIO
						return;
					}
					it++;
					}*/

	Pelicula* p = NULL;

	Pelicula* selp = NULL;

	list<Pelicula*>::iterator it = peliculas.begin();

	while (it != peliculas.end()) {

		p = *it;

		if (titulo.compare(p->getTitulo()) == 0) {

			selp = *it;
		}

		it++;
	}

	if (selp != NULL) {

		Cine* c = NULL;

		Cine* selc = NULL;

		list<Cine*> cin = selp->getCines();

		list<Cine*>::iterator it = cin.begin();

		while (it != cin.end()) {

			c = *it;

			if (cine == c->getNroCine()) {

				selc = *it;
			}

			it++;
		}

		if (selc != NULL) {

			Sala* s = NULL;

			Sala* sels = NULL;

			list<Sala*> salab = selc->getSalas();

			list<Sala*>::iterator it = salab.begin();

			while (it != salab.end()) {
				s = *it;
				if (sala == s->getNro()) {

					sels = *it;

				}

				it++;
			}

			Funcion* f = NULL;

			Funcion* self = NULL;

			list<Funcion*> funcionb = sels->getFuncion();

			list<Funcion*>::iterator itf = funcionb.begin();

			while (itf != funcionb.end()) {
							f = *itf;
							if (idFuncion == f->getNro()) {

								self = *itf;

							}

							itf++;
						}

				self->agregarReserva(cred);
		}

	} else {

		cout << "No existe la pelicula ingresada: " << titulo << endl;

	}

}


bool ControladorPelicula::valFinanciera(string financiera){
	list<Financiera*>::iterator it= financieras.begin();
	Financiera* f;
	while(it != financieras.end()){
					f = *it;
					if(f->getNombre().compare(financiera)==0){
						return true;
					}
					it++;
					}
	return false;
}






bool ControladorPelicula::estaComentada(string pel, string nick){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			return p->estaComentada(nick);
		}
		it++;
	}
	return false;
}

string ControladorPelicula::getComentario(string pel, string nick){
	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			return p->getComentario(nick);
		}
		it++;
	}
	return 0;
}

void ControladorPelicula::setComentario(string pel, string nick, string comentario){

	list<Pelicula*>::iterator it= peliculas.begin();
	Pelicula* p;
	p=peliculas.front();
	while(it != peliculas.end()){
		p = *it;
		if(pel.compare(p->getTitulo())==0){
			p->setComentario(nick, comentario);
			it = peliculas.end();
			it--;
		}
		it++;
	}
}



void ControladorPelicula::listaReservasUsuario(string nick) {

	bool hayreservas = false;

	Pelicula* p = NULL;

	list<Pelicula*>::iterator it = peliculas.begin();

	while (it != peliculas.end()) {

		p = *it;

		Cine* c = NULL;

		list<Cine*> cin = p->getCines();

		list<Cine*>::iterator itc = cin.begin();

		while (itc != cin.end()) {

			c = *itc;

			Sala* s = NULL;

			list<Sala*> salab = c->getSalas();

			list<Sala*>::iterator its = salab.begin();

			while (its != salab.end()) {

				s = *its;

				Funcion* f = NULL;

				list<Funcion*> funcionb = s->getFuncion();

				list<Funcion*>::iterator itf = funcionb.begin();

				while (itf != funcionb.end()) {

					f = *itf;
					Reserva* r;
					list<Reserva*> funcionr = f->getReserva();
					list<Reserva*>::iterator itr = funcionr.begin();

					while (itr != funcionr.end()) {
						r = *itr;
						if (nick.compare(r->getNick())== 0) {
							hayreservas = true;
							Credito* cr = NULL;
							Debito* d = NULL;


							d = dynamic_cast<Debito*>(r);
							cr = dynamic_cast<Credito*>(r);


							if (d != NULL) {
								cout << "Reservas usuario: " << d->getNick()
										<< endl;
								cout << "Pelicula: " << p->getTitulo() << endl;
								cout << "Cantidad de asientos: "
										<< d->getCantAsientos() << endl;
								cout << "Costo: " << d->getCosto() << endl;
								cout << "Forma de pago: Debito" << endl;
								cout << "Banco: " << d->getBanco() << endl;
								cout
										<< "----------------------------------------------"
										<< endl;
							} else {

								cout << "Reservas usuario: " << cr->getNick()
										<< endl;
								cout << "Pelicula: " << p->getTitulo() << endl;
								cout << "Cantidad de asientos: "
										<< cr->getCantAsientos() << endl;
								cout << "Costo: " << cr->getCosto() << endl;
								cout << "Forma de pago: Credito" << endl;
								cout << "Financiera: " << cr->getFinanciera()
										<< endl;
								cout << "Descuento: " << cr->getDescuento()
										<< endl;
								cout
										<< "----------------------------------------------"
										<< endl;

							}

						}
						itr ++;
					}

					itf++;
				}

				its++;
			}

			itc++;
		}

		it++;
	}



	if(hayreservas == false){

		cout<< "No hay reservas para este usuario!"<< endl;


	}

}


void ControladorPelicula::crearPeliculas(){

	Pelicula* p;
	string pel, desc, post;
	pel= "Jurassic-Park";
	desc= "During a preview tour, a theme park suffers a major power breakdown that allows its cloned dinosaur exhibits to run amok.";
	post= "https://m.media-amazon.com/images/M/MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_UX140_CR0,0,140,209_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;


	pel= "Interstellar";
	desc= "A team of explorers travel through a wormhole in space in an attempt to ensure humanity�s survival.";
	post= "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY209_CR0,0,140,209_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;

	pel= "Birdman";
	desc= "A washed-up superhero actor attempts to revive his fading career by writing, directing, and starring in a Broadway production.";
	post= "https://m.media-amazon.com/images/M/MV5BODAzNDMxMzAxOV5BMl5BanBnXkFtZTgwMDMxMjA4MjE@._V1_UY209_CR0,0,140,209_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se realizo carga de película: "<< p->getTitulo() << endl;

	pel= "The-Godfather";
	desc= "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.";
	post= "https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY209_CR3,0,140,209_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;

	pel= "Avengers:Endgame";
	desc= "After the devastating events, the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos actions and restore balance to the universe.";
	post= "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;

	pel= "Toy-Story-4";
	desc= "When a new toy called Forky joins Woody and the gang, a road trip alongside old and new friends reveals how big the world can be for a toy.";
	post= 'https://m.media-amazon.com/images/M/MV5BMTYzMDM4NzkxOV5BMl5BanBnXkFtZTgwNzM1Mzg2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg';
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;

	pel= "Captain-Marvel";
	desc= "Carol Danvers becomes one of the universe�s most powerful heroes when Earth is caught in the middle of a galactic war between two alien races.";
	post= "https://m.media-amazon.com/images/M/MV5BMTE0YWFmOTMtYTU2ZS00ZTIxLWE3OTEtYTNiYzBkZjViZThiXkEyXkFqcGdeQXVyODMzMzQ4OTI@._V1_UX182_CR0,0,182,268_AL_.jpg";
	p=new Pelicula(pel, post, desc, 0);
	this->peliculas.push_back(p);
	cout <<"Se hizo carga de película: "<< p->getTitulo() << endl;

	p->~Pelicula();
}

void ControladorPelicula::crearFinancieras(){
	Financiera* f;
	f=new Financiera("itau", 0.15);
	this->financieras.push_back(f);
	cout <<"Se hizo carga de financiera: "<< f->getNombre() << endl;

	f=new Financiera("brou", 0.1);
	this->financieras.push_back(f);
	cout <<"Se hizo carga de financiera: "<< f->getNombre() << endl;

	f=new Financiera("bbva", 0.2);
	this->financieras.push_back(f);
	cout <<"Se hizo carga de financiera: "<< f->getNombre() << endl;

	f=new Financiera("santander", 0.25);
	this->financieras.push_back(f);
	cout <<"Se hizo carga de financiera: "<< f->getNombre() << endl;
}

ControladorPelicula::~ControladorPelicula(){}


