
#include "../header/Funcion.h"

	Funcion::Funcion(){}

	Funcion::Funcion(int nroFuncion, dtFecha fecha, dtHora hora){

		this->nroFuncion = nroFuncion;
		this->fecha = fecha;
		this->hora = hora;

	}


	Funcion::~Funcion(){

	}


	int Funcion::getNro(){
		return this->nroFuncion;
	}

	dtFecha Funcion::getFecha(){
		return this->fecha;
	}

	dtHora Funcion::getHora(){
		return this->hora;
	}


	void Funcion::setNro(int nro){
		this->nroFuncion = nro;
	}

	void Funcion::setFecha(dtFecha fecha){
		this->fecha = fecha;
	}

	void Funcion::setHora(dtHora hora){
		this->hora = hora;
	}

	void Funcion::setReserva(list<Reserva*> reserva){

		this->reservas = reservas;

	}

	list<Reserva*> Funcion:: getReserva(){

		return this->reservas;
	}

	list<dtFuncion> Funcion::obtenerFuncion(){

	}

	void Funcion::agregarReserva(Reserva* reserva){

		this->reservas.push_back(reserva);

	}
