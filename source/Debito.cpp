/*
 * Debito.cpp
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */

#include "../header/Debito.h"

Debito::Debito() {

}

Debito::Debito(string banco) {
	this->banco = banco;
}

//getters
string Debito::getBanco(){
	return this->banco;
}

//setters
void Debito::setBanco(string banco){
	this->banco = banco;
}

Debito::~Debito() {

}

