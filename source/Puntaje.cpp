#include "../header/Puntaje.h"

Puntaje::Puntaje(){
	this->puntaje=-1;
	this->usuario=NULL;
}

Puntaje::Puntaje(int puntaje, Usuario* usuario){
	this->puntaje=puntaje;
	this->usuario=usuario;
}
Puntaje::~Puntaje(){}

int Puntaje::getPuntaje(){
	return this->puntaje;
}
void Puntaje::setPuntaje(int puntaje){
	this->puntaje=puntaje;
}

bool Puntaje::estaPuntuada(){
	if(this->usuario==NULL){
		return false;
	}else{
		return true;
	}
}

bool Puntaje::estaPuntuadaPor(string nick){
	if(nick.compare(this->usuario->getNick()) == 0){
		return true;
	}else{
		return false;
	}
}
