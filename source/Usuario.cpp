/*
 * Usuario.cpp
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */

#include "../header/Usuario.h"

Usuario::Usuario(){

}

Usuario::Usuario(string nick, string pass, string imagen, bool esAdmin){
		this->nick=nick;
		this->pass=pass;
		this->imagen=imagen;
		this->esAdmin=esAdmin;
}

Usuario::~Usuario(){

}

//getters
string Usuario::getNick(){
	return this->nick;
}
string Usuario::getPass(){
	return this->pass;
}
string Usuario::getImagen(){
	return this->imagen;
}
bool Usuario::getEsAdmin(){
	return this->esAdmin;
}

//setters
void Usuario::setNick(string nick){
	this->nick = nick;
}
void Usuario::setPass(string pass){
	this->pass = pass;
}
void Usuario::setImagen(string imagen){
	this->imagen = imagen;
}
void Usuario::setEsAdmin(bool esAdmin){
	this->esAdmin = esAdmin;
}
