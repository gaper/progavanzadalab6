/*
 * Reserva.cpp
 *
 *  Created on: 13 jun. 2019
 *      Author: Tomy
 */

#include "../header/Reserva.h"

Reserva::Reserva() {

}

Reserva::Reserva(int cantAsientos, float costo) {
	this->cantAsientos = cantAsientos;
	this->costo = costo;
}

//getters
int Reserva::getCantAsientos(){
	return this->cantAsientos;
}
float Reserva::getCosto(){
	return this->costo;
}

string Reserva::getNick(){

	return this->nick;

}

//setters
void Reserva::setCantAsientos(int cantAsientos){
	this->cantAsientos = cantAsientos;
}
void Reserva::setCosto(float costo){
	this->costo = costo;
}

void Reserva::setNick(string nick){

	this->nick = nick;
}


Reserva::~Reserva() {

}
