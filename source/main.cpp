#include "../header/Fabrica.h"
#include "../header/IControladorPelicula.h"
#include "../header/IControladorSesion.h"
#include "../header/IControladorReloj.h"

#include "../datatype/header/dtReloj.h"

#include "../header/Sesion.h"

#include <iostream>
#include <stdlib.h>
#include <list>
#include <string>
#include <ctime>

using namespace std;

IControladorPelicula *controladorPelicula = Fabrica::getInstancia()->getIControladorPelicula();
IControladorSesion *controladorSesion = Fabrica::getInstancia()->getIControladorSesion();
IControladorReloj *controladorReloj = Fabrica::getInstancia()->getIControladorReloj();

int cantCines = 0;

//Para el menu
void iniciarSesion();
void altaCine();
void altaFuncion();
void crearReserva();
void puntuarPelicula();
void comentarPelicula();
void eliminarPelicula();
void cerrarSesion();
void consultarReloj();
void modificarReloj();
void altaPeliculas();
void listarCines();
void listarFuncionPelicula();
void verInfoPelicula();
void verReservasUsuario();


//Funciones del sistema (letra)


//Auxiliares
int imprimirMenu();
int imprimirMenuInvitado();
int imprimirMenuUsuario();
int imprimirMenuAdmin();
void impHora(dtHora hora);
void impFecha(dtFecha fecha);

int main() {

	int seleccion;
	bool salir = false;

	cout << "*********BIENVENIDO A CINES***********" << endl;

	do{
		if(!controladorSesion->sesionIniciada()){
			seleccion = imprimirMenuInvitado();
			switch (seleccion){
			case 1 : iniciarSesion(); break;
			case 2 : verInfoPelicula(); break;
			case 3 : consultarReloj();  break;
			case 10 : salir = true;  break;
			default: cout << "[ERROR] Opcion invalida vuelva a ingresar " << endl;  break;
			}
		}else{
			if(controladorSesion->sesionEsAdmin(controladorSesion->getNickSesion())){
				seleccion = imprimirMenuAdmin();
				switch (seleccion){
				case 1 : altaCine();  break;
				case 2 : altaFuncion();  break;
				case 3 : listarCines();   break;
				case 4 : listarFuncionPelicula(); break;
				case 5 : altaPeliculas(); break;
				case 6 : consultarReloj();  break;
				case 7 : modificarReloj();  break;
				case 8 : cerrarSesion();  break;
				case 10 : salir = true;  break;
				default: cout << "[ERROR] Opcion invalida vuelva a ingresar " << endl;  break;
				}
			}else{
				seleccion = imprimirMenuUsuario();
				switch (seleccion){
				case 1 : crearReserva();  break;
				case 2 : verReservasUsuario(); break;
				case 3 : puntuarPelicula();  break;
				case 4 : comentarPelicula();  break;
				case 5 : verInfoPelicula(); break;
				case 6 : consultarReloj();  break;
				case 7 : cerrarSesion();  break;
				case 10 : salir = true;  break;
				default: cout << "[ERROR] Opcion invalida vuelva a ingresar " << endl;  break;
				}
			}

		}
		/*
		seleccion = imprimirMenu();
		switch (seleccion)
		{
		case 1 : iniciarSesion(); break;
		case 2 : altaCine();  break;
		case 3 : altaFuncion();  break;
		case 4 : crearReserva();  break;
		case 5 : puntuarPelicula();  break;
		case 6 : comentarPelicula();  break;
		case 7 : listarCines();   break;
		case 11 : listarFuncionPelicula(); break;
		case 8 : reloj();  break;
		case 9 : cerrarSesion();  break;
		case 10 : altaPeliculas(); break;

		case 12 : salir = true;  break;
		default: cout << "[ERROR] Opcion invalida vuelva a ingresar " << endl;  break;
		}
		*/
	} while (!salir);
	return 0;
}

int imprimirMenu() {
	int retorno;
	cin.clear();
	cout << "Selecciones un operacion: " << endl;
	cout << "1.Iniciar Sesion " << endl;
	cout << "2.Alta Cine " << endl;
	cout << "3.Alta Funcion" << endl;
	cout << "4.Crear Reserva" << endl;
	cout << "5.Puntuar Pelicula" << endl;
	cout << "6.Comentar Pelicula" << endl;
//	cout << "7.Listar Cines"<< endl;
	cout << "8.Reloj" << endl;
	cout << "9.Cerrar Sesion" << endl;
	cout << "10.Cargar Peliculas" << endl;
//	cout << "11.Listar funciones pelicula" << endl;
	cout << "12.Salir" << endl;
	cin >> retorno;
	return retorno;
}

int imprimirMenuInvitado() {
	int retorno;
	cin.clear();
	cout << "Bienvenido Invitado"<< endl;
	cout << "Seleccione un operacion: " << endl;
	cout << "1.Iniciar Sesion " << endl;
	cout << "2.Ver Informacion de Pelicula " << endl;
//	cout << "3.Ver Comentarios y Puntajes de Pel�cula" << endl;
	cout << "3.Consultar Reloj" << endl;
	cout << "10.Salir" << endl;
	cin >> retorno;
	return retorno;
}

int imprimirMenuUsuario() {
	int retorno;
	cin.clear();
	cout << "Bienvenido "<< controladorSesion->getNickSesion() << endl;
	cout << "Seleccione un operacion: " << endl;
	cout << "1.Crear Reserva" << endl;
	cout << "2.Ver Reservas" << endl;
	cout << "3.Puntuar Pelicula" << endl;
	cout << "4.Comentar Pelicula" << endl;
	cout << "5.Ver Informacion de Pelicula " << endl;
//	cout << "5.Ver Comentarios y Puntajes de Pel�cula" << endl;
///	cout << "7.Listar funciones de pelicula" << endl;
	cout << "6.Consultar Reloj" << endl;
	cout << "7.Cerrar Sesion" << endl;
	cout << "10.Salir" << endl;
	cin >> retorno;
	return retorno;
}

int imprimirMenuAdmin() {
	int retorno;
	cin.clear();
	cout << "Bienvenido "<< controladorSesion->getNickSesion() << endl;
	cout << "Admin"<< endl;
	cout << "Seleccione un operacion: " << endl;
	cout << "1.Alta Cine " << endl;
	cout << "2.Alta Funcion" << endl;
	cout << "3.Listar Cines"<< endl;
	cout << "4.Listar Funciones Pelicula" << endl;
	cout << "5.Cargar Datos(Peliculas/Financieras)" << endl;
	cout << "6.Consultar Reloj" << endl;
	cout << "7.Modificar Reloj" << endl;
	cout << "8.Cerrar Sesion" << endl;
	cout << "10.Salir" << endl;
	cin >> retorno;
	return retorno;
}

void altaCine(){

	if (!controladorSesion->sesionIniciada()) {
		char tecla;

		cin.clear();
		cout << "*********ALTA CINE**********" << endl;
		cout << "No esta logeado, debe iniciar sesion." << endl;
		cout << " Oprima cualquier tecla para volver al menu." << endl;
		cin >> tecla;

	} else {


	char salir;

	do {

		char salida;
		char cancelar;
		string calle;
		int cantsalas = 0;
		int puerta;
		int cantlugares;
		list<dtSala*> salas;
		list<dtFuncion*> funciones;
		dtDireccion d;
		cout << "*********ALTA CINE**********" << endl;
		cout << "--Direccion del Cine--" << endl;

		cout << "Nombre de la Calle:" << endl;
		cin >> calle;
		cout << "Nro de Puerta:" << endl;
		cin >> puerta;
		d = dtDireccion(calle, puerta);

		do {

			cout << "--Ingresar Salas--" << endl;

			cout << "Ingrese cantidad de lugares:" << endl;
			cin >> cantlugares;

			cantsalas++;
			dtSala* sala = new dtSala(cantsalas, cantlugares, funciones);
			salas.push_back(sala);

			cout << "Desea continuar cargando salas?" << endl;
			cout << "1 - Si" << endl;
			cout << "2 - No" << endl;
			cin >> salida;


		} while (salida == '1');

		cout << "Confirmar Cine?" << endl;
		cout << "1 - Si" << endl;
		cout << "2 - No" << endl;
		cin >> cancelar;

		if (cancelar == '1') {
			cantCines++;
			dtCine c = dtCine(cantCines, d, salas);
			controladorPelicula->altaCine(c);
		} else {

			cantCines = 0;
			cantsalas = 0;
			salas.clear();
			d.~dtDireccion();
		}

		cout << "Desea continuar cargando cines?" << endl;
		cout << "1 - Si" << endl;
		cout << "2 - No" << endl;
		cin >> salir;


	} while (salir == '1');

	}
}

void altaFuncion() {

	if (!controladorSesion->sesionIniciada()) {
		char tecla;

		cin.clear();
		cout << "*********ALTA FUNCION**********" << endl;
		cout << "No esta logeado, debe iniciar sesion." << endl;
		cout << " Oprima cualquier tecla para volver al menu." << endl;
		cin >> tecla;

	} else {

		string pel;

		int cine = 0;

		int cantfuncion = 0;

		int dia, mes, anio, horas, minutos, segundos, numsala;

		char salida;

		list<string> pelis = controladorPelicula->listarPeliculas();

		list<string> mostrarcine = controladorPelicula->listarCines();

		list<string>::iterator it = pelis.begin();

		list<string>::iterator itc = mostrarcine.begin();

		string p;

		if (pelis.size() == 0) {

			char tecla;
			cout << "No hay peliculas" << endl;
			cout << " Oprima cualquier tecla para volver al menu." << endl;
			cin >> tecla;

		} else {
			cout << "*********ALTA FUNCION**********" << endl;
			cout << "--Lista de Peliculas--" << endl;

			while (it != pelis.end()) {
				p = *it;
				cout << p << endl;
				it++;
			}

			cout << "Ingrese el nombre de pelicula" << endl;
			cin >> pel;

			if (mostrarcine.size() == 0) {

				char tecla;
				cout << "No hay cines ingresados" << endl;
				cout << " Oprima cualquier tecla para volver al menu." << endl;
				cin >> tecla;

			} else {

				cout << "--Lista de Cines--" << endl;

				while (itc != mostrarcine.end()) {
					p = *itc;
					cout << p << endl;
					itc++;
				}

				cout << "Ingrese el numero del cine" << endl;
				cin >> cine;

				do {

					list<dtSala*> sl = controladorPelicula->listarSalas(cine);

					list<dtSala*>::iterator itsa = sl.begin();

					dtSala* aux;

					while (itsa != sl.end()) {
						aux = *itsa;
						list<dtFuncion*> funs = aux->getFunciones();
						list<dtFuncion*>::iterator a = funs.begin();
						dtFuncion* x;

						if (funs.size() != 0) {
							cout << "Cine Nro: " << cine << endl;
							cout << "Capacidad: " << aux->getCapacidad()<< endl;
							cout << "Funciones Sala " << aux->getNroSala()
									<< ":" << endl;
							while (a != funs.end()) {
								x = *a;
								dtFecha d = x->getFecha();
								dtHora h = x->getHora();
								cout << d.getDia() << "/" << d.getMes() << "/"
										<< d.getAnio() << "-" << h.getHora()
										<< ":" << h.getMinuto() << ":"
										<< h.getSegundo() << endl;
								a++;
							}

						} else {
							cout << "Cine Nro: " << cine << endl;
							cout << "Capacidad: " << aux->getCapacidad()<< endl;
							cout << "Funciones Sala " << aux->getNroSala()
									<< ":" << endl;
							cout << "No hay funciones agregadas!" << endl;

						}
						itsa++;
					}

					cout << "Ingrese numero de la sala:" << endl;
					cin >> numsala;

					cout << "Igrese fecha de la funcion ej: 28/6/2019" << endl;
					cout << "Dia" << endl;
					cin >> dia;
					cout << "Mes" << endl;
					cin >> mes;
					cout << "Anio" << endl;
					cin >> anio;

					cout << "Igrese hora de la funcion ej: 15:0:0" << endl;
					cout << "Horas" << endl;
					cin >> horas;
					cout << "Minutos" << endl;
					cin >> minutos;
					cout << "Segundos" << endl;
					cin >> segundos;

					cantfuncion++;

					dtFecha nuevafecha = dtFecha(dia, mes, anio);
					dtHora nuevahora = dtHora(horas, minutos, segundos);

					dtFuncion nuevafun = dtFuncion(cantfuncion, nuevafecha,
							nuevahora);

					controladorPelicula->altaFuncion(pel, cine, numsala,
							nuevafun);

					cout << "Desea continuar cargando funciones?" << endl;
					cout << "1 - Si" << endl;
					cout << "2 - No" << endl;
					cin >> salida;


				} while (salida == '1');

			}
		}
	}
}

void iniciarSesion(){

	if(controladorSesion->sesionIniciada()){
		string tecla;

		cin.clear();
		cout << "*********INICIAR SESION**********" << endl;
		cout << controladorSesion->getNickSesion() << " esta logeado, debe cerrar sesion."<< endl;
		cout << " Oprima cualquier tecla para volver al menu."<< endl;
		cin >> tecla;

	}else{
		string nick;
		string pass;

		cin.clear();
		cout << "*********INICIAR SESION**********" << endl;
		cout << "Oprima 1 para salir o ingrese el nick: "<< endl;
		cin >> nick;

		while(nick.compare("1") != 0 && !controladorSesion->existeNick(nick)){
			cin.clear();
			cout << "*********INICIAR SESION**********" << endl;
			cout << "Nick incorrecto. Oprima 1 para salir o ingrese el nick nuevamente: "<< endl;
			cin >> nick;
		}
		if(nick.compare("1") != 0){
			cout << "Oprima 1 para salir o ingrese la contrasenia: "<< endl;
					cin >> pass;
					while(pass.compare("1") != 0 && !controladorSesion->passCorrecta(nick, pass)){
								cin.clear();
								cout << "*********INICIAR SESION**********" << endl;
								cout << "Pass incorrecta.  Oprima 1 para salir o ingrese la pass nuevamente: "<< endl;
								cin >> pass;
					}
					if(pass.compare("1") != 0){
						controladorSesion->iniciarSesion(nick);
					}
		}

	}
}

void crearReserva() {

	int opt;
	int cine = 0;
	bool hayfuncion = false;
	string titulo;
	list<string> listaP;
	list<int> listaC;
	//list<dtSala> funciones;
	listaP = controladorPelicula->listarPeliculas();
	list<string>::iterator i = listaP.begin();
	string imp;
	dtPelicula infoPel;

	do {
		cout << "Lista de peliculas: " << endl;
		while (i != listaP.end()) {
			imp = *i;

			cout << imp << endl;
			i++;
		}
		cout << "--------------------" << endl;

		cout << "Desea continual con la operacion?" << endl;
		cout << "1 - Si" << endl;
		cout << "2 - No" << endl;
		cin >> opt;
		if (opt == 2) {
			return;
		}
	} while (opt != 1);

	bool existe = false;
	do {
		cout << "Ingrese el titulo de la pelicula deseada: " << endl;
		cin >> titulo;
		list<string>::iterator i2 = listaP.begin();
		string tit;
		while (i2 != listaP.end()) {
			tit = *i2;
			if (tit.compare(titulo) == 0) {
				existe = true;
			}
			i2++;
		}
		if (!existe) {
			cout << "El titulo ingresado no existe. " << endl;
		}
	} while (!existe);

	infoPel = controladorPelicula->selPelicula(titulo);
	cout << "*********************Info Pelicula*********************" << endl;
	cout << "Titulo: " << infoPel.getTitulo() << endl;
	cout << "Sinopsis: " << infoPel.getSinopsis() << endl;
	cout << "Poster: " << infoPel.getPoster() << endl;
	cout << "Puntaje: " << infoPel.getPromedio() << endl;
	cout << "*******************************************************" << endl;

	listaC = controladorPelicula->verInfoAd(titulo);
	list<int>::iterator c = listaC.begin();
	int auxc;
	cout << "Lista de cines: " << endl;
	while (c != listaC.end()) {
		auxc = *c;

		cout << auxc << endl;
		c++;
	}
	cout << "----------------" << endl;
	cout << "                " << endl;
	cout << "Ingrese cine: " << endl;
	cin >> cine;
	//funciones = controladorPelicula->ingresarCine(cine, titulo);
	list<dtCine*> cins = controladorPelicula->listarSalasPeliculas(titulo);
	list<dtCine*>::iterator itc = cins.begin();
	dtCine* z;
	dtCine* cux;

	while (itc != cins.end()) {
					z = *itc;
					if(z->getNro() == cine){
						cux = *itc;

					}
			itc++;
	}
	list<dtSala*> sux = cux->getSalas();
	list<dtSala*>::iterator f = sux.begin();
	dtSala* fun;
	cout << "*********** Funciones *********** " << endl;
	while (f != sux.end()) {
		fun = *f;
		list<dtFuncion*> func = fun->getFunciones();
		list<dtFuncion*>::iterator z = func.begin();
		dtFuncion* fu;

		while (z != func.end()) {

			fu = *z;

			if (controladorReloj->consultarReloj() < fu->getReloj()) {

				hayfuncion = true;

				cout << "Sala: " << fun->getNroSala() << endl;
				cout << "Funcion: " << fu->getNro() << endl;
				cout << "Fecha: ";
				impFecha(fu->getFecha());
				cout << "Hora: ";
				impHora(fu->getHora());
				cout << "---------------------- " << endl;

			}

			z++;
		}
		f++;
	}
	if (hayfuncion == true) {

		cout << "********************************* " << endl;
		string banco;
		int cantAsientos, metPago, funcion, confirma, salab;
		float precio;

		cout << "Ingrese Sala: " << endl;
		cin >> salab;
		cout << "---------------------- " << endl;
		cout << "                       " << endl;

		cout << "Ingrese Funcion: " << endl;
		cin >> funcion;
		cout << "---------------------- " << endl;
		cout << "                       " << endl;

		cout << "Ingrese cantidad de asientos: " << endl;
		cin >> cantAsientos;
		cout << "---------------------- " << endl;
		cout << "                       " << endl;

		cout << "Ingrese metodo de pago: " << endl;
		cout << "1 - Debito" << endl;
		cout << "2 - Credito" << endl;
		cin >> metPago;
		cout << "---------------------- " << endl;
		cout << "                       " << endl;

		if (metPago == 1) {
			cout << "Ingrese nombre del banco: " << endl;
			cin >> banco;
			precio = controladorPelicula->verPrecio(cantAsientos);
			cout << "Precio de la reserva: " << precio << endl;
			cout << "                          " << endl;
			cout << "                          " << endl;
			cout << "Confirma reserva? (1=Si 2=No): " << endl;
			cin >> confirma;
			if (confirma == 1) {
				string us = controladorSesion->getNickSesion();

				controladorPelicula->confirmarD(funcion, cantAsientos, banco,
						titulo, us, salab, cine);
			}

		} else if (metPago == 2) {
			cout << "Ingrese nombre de la financiera: " << endl;
			cin >> banco;
			if (controladorPelicula->valFinanciera(banco)) {
				precio = controladorPelicula->verPrecioC(banco, cantAsientos);
				cout << "Precio de la reserva: " << precio << endl;
				cout << "                          " << endl;
				cout << "                          " << endl;
				cout << "Confirma reserva? (1=Si 2=No): " << endl;
				cin >> confirma;
				if (confirma == 1) {
					string us = controladorSesion->getNickSesion();
					controladorPelicula->confirmarC(funcion, cantAsientos,
							banco, titulo, us, salab, cine);
				}
			} else {
				cout << "No existe esa financiera " << endl;
				return;
			}

		}
	}
}

void puntuarPelicula(){
	if(!controladorSesion->sesionIniciada()){
		string tecla;

		cin.clear();
		cout << "*********PUNTUAR PELICULA**********" << endl;
		cout << "No esta logeado, debe iniciar sesion."<< endl;
		cout << " Oprima cualquier tecla para volver al menu."<< endl;
		cin >> tecla;

	}else{

		string pel;
		list<string> pelis = controladorPelicula->listarPeliculas();
		list<string>::iterator it= pelis.begin();
		string p;

		cin.clear();
		cout << "*********PUNTUAR PELICULA**********" << endl;

		if(it == pelis.end()){
			char tecla;
			cout << "No hay peliculas" << endl;
			cout << " Oprima cualquier tecla para volver al menu."<< endl;
			cin >> tecla;

		}else{
			while(it != pelis.end()){
				p = *it;
				cout << p << endl;
				it++;
			}
			cout << "Oprima 1 para salir o ingrese el nombre de la pelicula: " << endl;
			cin >> pel;
			while(pel.compare("1") != 0 && !controladorPelicula->existePelicula(pel)){
				cin.clear();
				cout << "*********PUNTUAR PELICULA**********" << endl;
				cout << "Pelicula incorrecta. Oprima 1 para salir o ingrese la pelicula nuevamente: "<< endl;
				cin >> pel;
			}
			if(pel.compare("1") != 0){
				if (controladorPelicula->estaPuntuada(pel, controladorSesion->getNickSesion())){
					char aux;
					cin.clear();
					cout << "*********PUNTUAR PELICULA**********" << endl;
					cout << "La pelicula esta puntuada con: " << controladorPelicula->getPuntaje(pel, controladorSesion->getNickSesion()) << endl;
					cout << "Oprima 1 para modificar el puntaje o otra tecla para salir."<< endl;
					cin >> aux;
					if (aux == '1'){
						int punt;
						cin.clear();
						cout << "*********PUNTUAR PELICULA**********" << endl;
						cout << "Ingrese el nuevo puntaje:"<< endl;
						cin >> punt;
						controladorPelicula->setPuntaje(pel, controladorSesion->getNickSesion(), punt);
					}
				}else{
					int punt;
					cin.clear();
					cout << "*********PUNTUAR PELICULA**********" << endl;
					cout << "Ingrese el nuevo puntaje:"<< endl;
					cin >> punt;
					controladorPelicula->setPuntaje(pel, controladorSesion->getNickSesion(), punt);
				}
			}
		}

	}
}

void comentarPelicula(){
	if(!controladorSesion->sesionIniciada()){
		string tecla;

		cin.clear();
		cout << "*********COMENTAR PELICULA**********" << endl;
		cout << "No esta logeado, debe iniciar sesion."<< endl;
		cout << " Oprima cualquier tecla para volver al menu."<< endl;
		cin >> tecla;

	}else{

		string pel;
		list<string> pelis = controladorPelicula->listarPeliculas();
		list<string>::iterator it= pelis.begin();
		string p;

		cin.clear();
		cout << "*********COMENTAR PELICULA**********" << endl;

		if(it == pelis.end()){
			char tecla;
			cout << "No hay peliculas" << endl;
			cout << " Oprima cualquier tecla para volver al menu."<< endl;
			cin >> tecla;

		}else{
			while(it != pelis.end()){
				p = *it;
				cout << p << endl;
				it++;
			}
			cout << "Oprima 1 para salir o ingrese el nombre de la pelicula: " << endl;
			cin >> pel;
			while(pel.compare("1") != 0 && !controladorPelicula->existePelicula(pel)){
				cin.clear();
				cout << "*********COMENTAR PELICULA**********" << endl;
				cout << "Pelicula incorrecta. Oprima 1 para salir o ingrese la pelicula nuevamente: "<< endl;
				cin >> pel;
			}
			if(pel.compare("1") != 0){
				if (controladorPelicula->estaComentada(pel, controladorSesion->getNickSesion())){
					char aux;
					cin.clear();
					cout << "*********COMENTAR PELICULA**********" << endl;
					cout << "La pelicula esta comentada con: " << controladorPelicula->getComentario(pel, controladorSesion->getNickSesion()) << endl;
					cout << "Oprima 1 para modificar el comentario o otra tecla para salir."<< endl;
					cin >> aux;
					if (aux == '1'){
						string com;
						cin.clear();
						cout << "*********COMENTAR PELICULA**********" << endl;
						cout << "Ingrese el nuevo comentario:"<< endl;
						cin >> com;
						controladorPelicula->setComentario(pel, controladorSesion->getNickSesion(), com);
					}
				}else{
					string com;
					cin.clear();
					cout << "*********COMENTAR PELICULA**********" << endl;
					cout << "Ingrese el nuevo comentario:"<< endl;
					cin >> com;
					controladorPelicula->setComentario(pel, controladorSesion->getNickSesion(), com);
				}
			}
		}

	}
}

void eliminarPelicula(){}
void cerrarSesion(){

	if(!controladorSesion->sesionIniciada()){
		string tecla;

		cin.clear();
		cout << "*********CERRAR SESION**********" << endl;
		cout << "No esta logeado, debe iniciar sesion."<< endl;
		cout << " Oprima cualquier tecla para volver al menu."<< endl;
		cin >> tecla;

	}else{
		char aux;
		cin.clear();
		cout << "*********CERRAR SESION**********" << endl;
		cout << controladorSesion->getNickSesion() << " esta logeado, oprima 1 para cerrar sesion."<< endl;
		cout << " Oprima cualquier tecla para volver al menu."<< endl;
		cin >> aux;
		if(aux == '1'){
			controladorSesion->cerrarSesion();
		}
	}
}

void reloj(){
	char aux;
	cin.clear();
	cout << "*********RELOJ**********" << endl;
	cout << "Oprima 1 para modificar el reloj, 2 para ver el reloj o otra tecla para salir."<< endl;
	cin >> aux;


}

void consultarReloj(){
	char aux;
	dtReloj r=controladorReloj->consultarReloj();

	cin.clear();
	cout << "*********CONSULTAR RELOJ**********" << endl;
	cout << "Reloj: " << r.getDia() << "/" << r.getMes() << "/" << r.getAnio()<< " " << r.getHora() << ":" << r.getMinutos() << endl;
	cout << "Oprima cualquier tecla para volver al menu."<< endl;
	cin >> aux;
}
void modificarReloj(){
	char aux;
	int d, m, a, h, min;

	cout << "*********MODIFICAR RELOJ**********" << endl;
	cout << "Ingrese Dia: "<< endl;
	cin >> d;
	cout << "Ingrese Mes: "<< endl;
	cin >> m;
	cout << "Ingrese Anio: "<< endl;
	cin >> a;
	cout << "Ingrese Hora: "<< endl;
	cin >> h;
	cout << "Ingrese Minutos: "<< endl;
	cin >> min;

	dtReloj r = dtReloj(d,m,a,h,min);
	controladorReloj->modificarReloj(r);

	r=controladorReloj->consultarReloj();

	cout << "Reloj modificado: " << r.getDia() << "/" << r.getMes() << "/" << r.getAnio()<< " " << r.getHora() << ":" << r.getMinutos() << endl;
	cout << "Oprima cualquier tecla para volver al menu."<< endl;
	cin >> aux;

}

void altaPeliculas(){
	controladorPelicula->crearPeliculas();
	controladorPelicula->crearFinancieras();
}

void impHora(dtHora hora){
	cout <<hora.getHora() <<":" << hora.getMinuto() << ":" << hora.getSegundo()<< endl;

}

void impFecha(dtFecha fecha){
	cout <<fecha.getDia() <<"/" << fecha.getMes() << "/" << fecha.getAnio()<< endl;

}

void listarCines(){


	int cine = 0;

	list<string> mostrarcine = controladorPelicula->listarCines();

	list<string>::iterator itc= mostrarcine.begin();

	string p;

	cout <<"**Cines**"<< endl;

	while(itc != mostrarcine.end()){
		    	p = *itc;
		    	cout << p << endl;
		    	itc++;
	}

	cout <<"Inregese numero del cine:"<< endl;
	cin >> cine;

	list<dtSala*> sl = controladorPelicula->listarSalas(cine);

	list<dtSala*>::iterator itsa= sl.begin();

	dtSala* aux;



		while(itsa != sl.end()){
		    	aux = *itsa;
		    	list<dtFuncion*> funs = aux->getFunciones();
		    	list<dtFuncion*>::iterator a= funs.begin();
		    	dtFuncion* x;



		    	if(funs.size() != 0){

		    	cout <<"Funciones Sala" << aux->getNroSala() << ":" << endl;
		    	while(a != funs.end()){
		    		    	x = *a;
		    		    	dtFecha d =  x->getFecha();
		    		    	dtHora h = x->getHora();
		    		    	cout << d.getDia() << "/" << d.getMes() << "/" << d.getAnio() << "-" <<  h.getHora() << ":" << h.getMinuto() << ":" << h.getSegundo()<< endl;
		    		    	a++;

		    	}
		    	}else{

		    				cout <<"Funciones Sala " << aux->getNroSala() << ":" << endl;
		    				cout <<"No hay funciones agregadas!"<< endl;

		    			    }
		    	itsa++;
		}


}

void listarFuncionPelicula() {

	string pel;

	list<string> pelis = controladorPelicula->listarPeliculas();

	list<string>::iterator it = pelis.begin();

	string p;

	if (pelis.size() == 0) {

		char tecla;
		cout << "No hay peliculas" << endl;
		cout << " Oprima cualquier tecla para volver al menu." << endl;
		cin >> tecla;

	} else {

		cout << "--Lista de Peliculas--" << endl;

		while (it != pelis.end()) {
			p = *it;
			cout << p << endl;
			it++;
		}

		cout << "Ingrese el nombre de pelicula" << endl;
		cin >> pel;

		list<dtCine*> cl = controladorPelicula->listarSalasPeliculas(pel);

		if (cl.size() == 0) {

			char tecla;
			cout << "No hay peliculas" << endl;
			cout << " Oprima cualquier tecla para volver al menu." << endl;
			cin >> tecla;

		} else {

			list<dtCine*>::iterator itc = cl.begin();

			dtCine* c;

			while (itc != cl.end()) {
				c = *itc;

				list<dtSala*> sl = c->getSalas();

				list<dtSala*>::iterator itsa = sl.begin();

				dtSala* aux;

				while (itsa != sl.end()) {
					aux = *itsa;
					list<dtFuncion*> funs = aux->getFunciones();
					list<dtFuncion*>::iterator a = funs.begin();
					dtFuncion* x;

					if (funs.size() != 0) {

						cout << "Pelicula: " << pel << endl;
						cout << "Cine Nro: " << c->getNro() << endl;
						cout << "Funciones Sala " << aux->getNroSala() << ":"
								<< endl;
						while (a != funs.end()) {
							x = *a;
							dtFecha d = x->getFecha();
							dtHora h = x->getHora();
							cout << d.getDia() << "/" << d.getMes() << "/"
									<< d.getAnio() << "-" << h.getHora() << ":"
									<< h.getMinuto() << ":" << h.getSegundo()
									<< endl;
							a++;

						}
					} else {
						cout << "Pelicula: " << pel << endl;
						cout << "Cine Nro: " << c->getNro() << endl;
						cout << "Funciones Sala " << aux->getNroSala() << ":"
								<< endl;
						cout << "No hay funciones agregadas!" << endl;

					}
					itsa++;
				}

				itc++;
			}
		}
	}
}

void verInfoPelicula(){

			char repetir;
			string tecla;
			string pel;
			int num;
			list<string> pelis;
			list<string>::iterator it;
			string p;
			list<dtSala> funciones;

			do{
				pelis = controladorPelicula->listarPeliculas();
				it= pelis.begin();

				cin.clear();
				cout << "*********VER INFORMACION PELICULA**********" << endl;

				if(it == pelis.end()){
					char tecla;
					cout << "No hay peliculas" << endl;
					cout << " Oprima cualquier tecla para volver al menu."<< endl;
					cin >> tecla;

				}else{
					while(it != pelis.end()){
						p = *it;
						cout << p << endl;
						it++;
					}
					dtPelicula pels;
					pels = controladorPelicula->selPelicula(pel);
					cout << "Oprima 1 para salir o ingrese el nombre de la pelicula: " << endl;
					cin >> pel;
					if(pel.compare("1") != 0 && controladorPelicula->existePelicula(pel)){
						cin.clear();
						cout << "*********VER INFORMACION PELICULA**********" << endl;
						cout << "Poster: " << pels.getPoster() << endl;
						cout << "Sinopsis: " << pels.getSinopsis() << endl;
						cout << " Oprima cualquier tecla para continuar."<< endl;
						cin >> tecla;
					}
					cout << "Desea ver informacion adicional?" << endl;
					cout << "1 - Si" << endl;
					cout << "2 - No" << endl;
					cin >> num;

					if(num == '1'){

						list<int> cines = controladorPelicula->verInfoAd(pel);
						list<int>::iterator it= cines.begin();
						string c;

						cin.clear();
						cout << "*********VER INFORMACION PELICULA**********" << endl;
						cout << "Lista de cines: " << endl;

						while(it != cines.end()){
							c = *it;
							cout << c << endl;
							it++;
						}

						cout << "Oprima 0 para salir, o ingrese el numero del cine: " << endl;
						cin >> num;

						dtFuncion* funciones;
						// falta hacer la lista de funciones
						if(num != 0 && funciones->getReloj() < controladorReloj->consultarReloj()){
							cout << "*********VER INFORMACION ADICIONAL**********" << endl;
							cout << "Lista de funciones: " << endl;
							cin >> num;
						}else{
							cout << "*********VER INFORMACION ADICIONAL**********" << endl;
							cout << "No hay funciones posteriores a la fecha actual. " << endl;
							cout << "Oprima cualquier tecla para continuar. " << endl;
							cin >> num;
						}
					}
				}
				cin.clear();
				cout << "*********VER INFORMACION PELICULA**********" << endl;
				cout << "Oprima 1 para repetir o cualquier otra tecla para volver al menu " << endl;
				cin >> repetir;
			}while(repetir == '1');


}



void verReservasUsuario(){

string n = controladorSesion->getNickSesion();

controladorPelicula->listaReservasUsuario(n);

}
