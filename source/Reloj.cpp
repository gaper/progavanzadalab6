#include "../header/Reloj.h"
#include <string>
using namespace std;

Reloj:: Reloj(){
	this->anio=0;
	this->mes=0;
	this->dia=0;
	this->hora=0;
	this->minutos=0;
}

Reloj* Reloj::instance = 0;

Reloj* Reloj:: getInstancia(){
	if (instance == NULL){
		instance = new Reloj();
	}
	return instance;
}

Reloj::~Reloj(){}

void Reloj:: setDia(int dia){
    this->dia=dia;
}
void Reloj:: setMes(int mes){
    this->mes=mes;
}
void Reloj:: setAnio(int anio){
    this->anio=anio;
}
void Reloj:: setHora(int hora){
    this->hora=hora;
}
void Reloj:: setMinutos(int minutos){
    this->minutos=minutos;
}

int Reloj:: getDia(){
    return this->dia;
}
int Reloj:: getMes(){
    return this->mes;
}
int Reloj:: getAnio(){
    return this->anio;
}
int Reloj:: getHora(){
    return this->hora;
}
int Reloj:: getMinutos(){
    return this->minutos;
}
