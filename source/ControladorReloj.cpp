#include "../header/ControladorReloj.h"

ControladorReloj* ControladorReloj::instancia = NULL;

ControladorReloj::ControladorReloj(){}

ControladorReloj* ControladorReloj::getInstancia(){

	if (instancia == NULL)
	instancia = new ControladorReloj();
	return instancia;
}

void ControladorReloj::modificarReloj (dtReloj reloj){
	Reloj::getInstancia()->setDia(reloj.getDia());
	Reloj::getInstancia()->setMes(reloj.getMes());
	Reloj::getInstancia()->setAnio(reloj.getAnio());
	Reloj::getInstancia()->setHora(reloj.getHora());
	Reloj::getInstancia()->setMinutos(reloj.getMinutos());
}

dtReloj ControladorReloj::consultarReloj (){
	dtReloj r = dtReloj(Reloj::getInstancia()->getDia(),Reloj::getInstancia()->getMes(),Reloj::getInstancia()->getAnio(),Reloj::getInstancia()->getHora(),Reloj::getInstancia()->getMinutos());
	return r;
}

ControladorReloj::~ControladorReloj(){}


