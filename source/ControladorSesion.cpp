#include "../header/ControladorSesion.h"
#include <iostream>
#include <string>
using namespace std;

ControladorSesion* ControladorSesion::instancia = NULL;

ControladorSesion::ControladorSesion(){}

ControladorSesion* ControladorSesion::getInstancia(){

	if (instancia == NULL)
	instancia = new ControladorSesion();
	return instancia;
}


void ControladorSesion::ingresarNick (string nick){
	Sesion::getInstancia()->setNick(nick);
}

void ControladorSesion::ingresarPass (string pass){}

bool ControladorSesion::sesionIniciada (){
	string nick;
	nick=Sesion::getInstancia()->getNick();
	if(nick.compare("") != 0){
		return true;
	}else{
		return false;
	}
}

string ControladorSesion::getNickSesion (){
	return Sesion::getInstancia()->getNick();
}

bool ControladorSesion::existeNick(string nick){
	if(ManejadorUsuario::getInstancia()->existeUsuario(nick)){
		return true;
	}else{
		return false;
	}
}

bool ControladorSesion::passCorrecta(string nick,string pass){

	Usuario* u = ManejadorUsuario::getInstancia()->buscarUsuario(nick);
	if(pass.compare(u->getPass()) == 0){
		return true;
	}else{
		return false;
	}
}

void ControladorSesion::iniciarSesion (string nick){
	Sesion::getInstancia()->setNick(nick);
}

bool ControladorSesion::sesionEsAdmin (string nick){
	Usuario* u = ManejadorUsuario::getInstancia()->buscarUsuario(nick);
	return u->getEsAdmin();
}

void ControladorSesion:: cerrarSesion(){
	Sesion::getInstancia()->setNick("");
}

ControladorSesion::~ControladorSesion(){}
