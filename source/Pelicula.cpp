

#include "../header/Pelicula.h"

	Pelicula::Pelicula(){}

	Pelicula::Pelicula(string titulo, string poster, string sinopsis, float puntajePromedio){
		this->puntajePromedio=puntajePromedio;
		this->titulo=titulo;
		this->poster=poster;
		this->sinopsis=sinopsis;
	}

	Pelicula::~Pelicula(){

	}

	//gettes
	string Pelicula::getTitulo(){
		return this->titulo;


	}

	string Pelicula::getPoster(){
		return this->poster;

	}

	string Pelicula::getSinopsis(){
		return this->sinopsis;

	}


	float Pelicula::getPromedio(){
		return this->puntajePromedio;
	}

	list<Cine*> Pelicula::getCines(){
		return this->cines;
	}

	/*list<Funcion*> Pelicula::getFunciones(){
		return this->funciones;
	}*/

	list<Puntaje*> Pelicula::getPuntajes(){
		return this->puntajes;
	}

	list<Comentario*> Pelicula::getComentarios(){
			return this->comentarios;
	}

	//setters

	void Pelicula::setPuntajePromedio(float puntajePromedio){
		this->puntajePromedio=puntajePromedio;
	}

	void Pelicula::setSinopsis(string sinopsis){
		this->sinopsis=sinopsis;

	}

	void Pelicula::setTitulo(string titulo){
		this->titulo=titulo;
	}

	void Pelicula::setCines(list<Cine*> cines){
		this->cines=cines;

	}

	/*void Pelicula::setFunciones(list<Funcion*> funciones){
		this->funciones=funciones;

	}*/

	void Pelicula::setPuntajes(list<Puntaje*> puntajes){
		this->puntajes=puntajes;
	}

	void Pelicula::setComentarios(list<Comentario*> comentarios){
		this->comentarios=comentarios;
	}

	void Pelicula::setPoster(string poster){
		this->poster=poster;
	}

bool Pelicula::estaPuntuada(string nick){
	list<Puntaje*>::iterator it= this->puntajes.begin();
	Puntaje* p;
	while(it != this->puntajes.end()){
			p = *it;
			if(p->estaPuntuadaPor(nick)){
				return true;
			}
			it++;
	}
	return false;
}

int Pelicula::getPuntaje(string nick){
	list<Puntaje*>::iterator it= this->puntajes.begin();
	Puntaje* p;
	while(it != this->puntajes.end()){
			p = *it;
			if(p->estaPuntuadaPor(nick)){
				return p->getPuntaje();
			}
			it++;
	}
	return 0;
}

void Pelicula::setPuntaje(string nick, int puntaje){
	bool existePuntaje=false;
	list<Puntaje*>::iterator it= this->puntajes.begin();
	Puntaje* p;
	while(it != this->puntajes.end()){
			p = *it;
			if(p->estaPuntuadaPor(nick)){
				p->setPuntaje(puntaje);
				it = this->puntajes.end();
				it--;
				existePuntaje=true;
			}
			it++;
	}
	if(!existePuntaje){
		Usuario* u = ManejadorUsuario::getInstancia()->buscarUsuario(nick);
		Puntaje* auxP = new Puntaje(puntaje,u);
		puntajes.push_back(auxP);
	}
}

bool Pelicula::estaComentada(string nick){
	list<Comentario*>::iterator it= this->comentarios.begin();
	Comentario* c;
	while(it != this->comentarios.end()){
			c = *it;
			if(c->estaComentadaPor(nick)){
				return true;
			}
			it++;
	}
	return false;
}

string Pelicula::getComentario(string nick){
	list<Comentario*>::iterator it= this->comentarios.begin();
	Comentario* c;
	while(it != this->comentarios.end()){
			c = *it;
			if(c->estaComentadaPor(nick)){
				return c->getComentario();
			}
			it++;
	}
	return "";
}

void Pelicula::setComentario(string nick, string comentario){
	bool existeComentario=false;
	list<Comentario*>::iterator it= this->comentarios.begin();
	Comentario* c;
	while(it != this->comentarios.end()){
			c = *it;
			if(c->estaComentadaPor(nick)){
				c->setComentario(comentario);
				it = this->comentarios.end();
				it--;
				existeComentario=true;
			}
			it++;
	}
	if(!existeComentario){
		Usuario* u = ManejadorUsuario::getInstancia()->buscarUsuario(nick);
		Comentario* auxC = new Comentario(comentario,u);
		comentarios.push_back(auxC);
	}
}

void Pelicula::agregarCine(Cine* c){

	this->cines.push_back(c);

}


dtPelicula Pelicula::selPelicula(){
		dtPelicula pel;
		pel= dtPelicula(this->titulo, this->poster, this->sinopsis, this->puntajePromedio);
		return pel;
	}

list<int> Pelicula::verInfoAd(){
		list<Cine*>::iterator it= this->cines.begin();
		Cine* c;
		int cine;
		list<int> cines;
		while(it != this->cines.end()){
				c = *it;
				cine=c->getNroCine();
				cines.push_back(cine);
				it++;
		}
		return cines;
	}


list<dtSala> Pelicula::ingresarCine(int nroCine){
		list<Cine*>::iterator it = cines.begin();
		Cine* c;
		list<dtSala> funcion;
		while(it != cines.end()){
				c = *it;
				if(c->getNroCine()==nroCine){
					funcion=c->obtenerFuncion();
					return funcion;
				}
				it++;
		}

}

