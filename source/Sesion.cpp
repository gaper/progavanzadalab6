#include "../header/Sesion.h"
#include <string>
using namespace std;

Sesion:: Sesion(){
    this->nick="";
}

Sesion* Sesion::instance = 0;

Sesion* Sesion:: getInstancia(){
    if (instance == NULL){
        instance = new Sesion();
    }
    return instance;
}

void Sesion:: setNick(string nick){
    this->nick=nick;
}
string Sesion::getNick(){
    return this->nick;
}

Sesion:: ~Sesion(){}
