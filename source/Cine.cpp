#include "../header/Cine.h"
#include "../datatype/header/dtDireccion.h"
#include "../datatype/header/dtFuncion.h"

Cine::Cine() {
}

Cine::Cine(int nroCine, dtDireccion direccion) {

	this->nroCine = nroCine;

	this->direccion = direccion;

}

Cine::~Cine() {
}

int Cine::getNroCine() {

	return this->nroCine;

}

list<Sala*> Cine::getSalas() {

	return this->salas;
}

dtDireccion Cine::getDireccion() {

	return this->direccion;
}

void Cine::setNroCine(int nroCine) {

	this->nroCine = nroCine;
}

void Cine::setDireccion(dtDireccion direccion) {

	this->direccion = direccion;

}

void Cine::setSalas(list<Sala*> salas) {

	this->salas = salas;

}

void Cine::agregarSala(Sala* s){

	this->salas.push_back(s);
}

list<dtSala> Cine::obtenerFuncion() {
	//No le esta mandando la funcion obtenerFuncion a la clase funcion

	list<dtSala> salasdev;
	list<Sala*>::iterator it = salas.begin();
	Sala* s;
	list<dtFuncion*> funcion;

	while(it != salas.end()){
			s = *it;

			list<Funcion*> fun= s->getFuncion();
			list<Funcion*>::iterator fit= fun.begin();
			Funcion* fu;

			while(fit != fun.end()){
				fu=*fit;
				dtFuncion* aux= new dtFuncion(fu->getNro(), fu->getFecha(), fu->getHora());
				funcion.push_back(aux);
				fit++;
			}

			dtSala sal = dtSala(s->getNro(),s->getCapacidad(),funcion);

			salasdev.push_back(sal);

			it++;
	}
	return salasdev;
}
