/*
 * dtPelicula.cpp
 *
 *  Created on: Jun 17, 2019
 *      Author: gaston
 */

#include "../header/dtPelicula.h"

dtPelicula::dtPelicula(){

	}

dtPelicula::dtPelicula(string titulo, string poster, string sinopsis, float puntajePromedio){

		this->titulo = titulo;
		this->poster = poster;
		this->sinopsis = sinopsis;
		this->puntajePromedio = puntajePromedio;

}

dtPelicula::~dtPelicula(){


}

string dtPelicula::getTitulo(){

	return this->titulo;
}

string dtPelicula::getPoster(){
	return this->poster;
}

string dtPelicula::getSinopsis(){

	return this->sinopsis;
}

float dtPelicula::getPromedio(){

	return this->puntajePromedio;
}

