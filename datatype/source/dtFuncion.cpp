/*
 * dtFuncion.cpp
 *
 *  Created on: Jun 17, 2019
 *      Author: gaston
 */

#include "../header/dtFuncion.h"



	dtFuncion::dtFuncion(int nroFuncion, dtFecha fecha, dtHora hora){

		this->nroFuncion = nroFuncion;
		this->fecha = fecha;
		this->hora = hora;

	}

	dtFuncion::~dtFuncion(){}
	//DtConsulta(DtFecha fecha, string motivo);

	//gettes

	int dtFuncion::getNro(){

		return this->nroFuncion;
	}
	dtFecha dtFuncion::getFecha(){
		return this->fecha;
	}

	dtHora dtFuncion::getHora(){

		return this->hora;

	}

	dtReloj dtFuncion::getReloj(){

		dtReloj r = dtReloj(this->fecha.getDia(),this->fecha.getMes(),this->fecha.getAnio(),this->hora.getHora(),this->hora.getMinuto());
		return r;

	}
