#include "../header/dtReloj.h"

dtReloj:: dtReloj(){
	this->anio=0;
	this->mes=0;
	this->dia=0;
	this->hora=0;
	this->minutos=0;
}

dtReloj:: dtReloj(int d, int m, int a, int h, int min){
	this->anio=a;
	this->mes=m;
	this->dia=d;
	this->hora=h;
	this->minutos=min;
}

dtReloj::~dtReloj(){}

int dtReloj:: getDia(){
    return this->dia;
}
int dtReloj:: getMes(){
    return this->mes;
}
int dtReloj:: getAnio(){
    return this->anio;
}
int dtReloj:: getHora(){
    return this->hora;
}
int dtReloj:: getMinutos(){
    return this->minutos;
}

bool operator < (const dtReloj & f1, const dtReloj & f2){
	if(f1.anio < f2.anio){
		return true;
	}else{
		if(f1.anio > f2.anio)	return false;
		if(f1.mes < f2.mes){
			return true;
		}else{
			if(f1.mes > f2.mes) return false;
			if(f1.dia < f2.dia){
				return true;
			}else{
				if(f1.dia > f2.dia) return false;
				if(f1.hora < f2.hora){
					return true;
				}else{
					if(f1.minutos > f2.minutos) return false;
					return false;
				}
			}
		}
	}
}

