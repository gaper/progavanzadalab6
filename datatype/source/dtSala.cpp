/*
 * dtSala.cpp
 *
 *  Created on: Jun 19, 2019
 *      Author: gaston
 */

#include "../header/dtSala.h"

dtSala::dtSala() {
}

dtSala::dtSala(int nroSala, int capacidad, list<dtFuncion*> funciones) {
		this->nroSala = nroSala;
		this->capacidad = capacidad;
		this->funciones = funciones;
}

int dtSala::getCapacidad(){
	return capacidad;
}


list<dtFuncion*> dtSala::getFunciones(){
	return funciones;
}

int dtSala::getNroSala(){
	return nroSala;
}


