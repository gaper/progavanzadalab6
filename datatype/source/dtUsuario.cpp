#include "../header/dtUsuario.h"

dtUsuario::dtUsuario(){

}

dtUsuario::dtUsuario(string nick, string pass, string imagen, bool esAdmin){
		this->nick=nick;
		this->pass=pass;
		this->imagen=imagen;
		this->esAdmin=esAdmin;
}

dtUsuario::~dtUsuario(){

}

//getters
string dtUsuario::getNick(){
	return this->nick;
}
string dtUsuario::getPass(){
	return this->pass;
}
string dtUsuario::getImagen(){
	return this->imagen;
}
bool dtUsuario::getEsAdmin(){
	return this->esAdmin;
}

