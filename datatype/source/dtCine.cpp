/*
 * dtCine.cpp
 *
 *  Created on: Jun 17, 2019
 *      Author: gaston
 */

#include "../header/dtCine.h"


dtCine::dtCine(int nroCine, dtDireccion direccion,list<dtSala*> salas){

		this->nroCine = nroCine;
		this->direccion = direccion;
		this->salas = salas;
}

dtCine::~dtCine(){

}

int dtCine::getNro(){

	return this->nroCine;
}

dtDireccion dtCine::getDireccion(){

	return this->direccion;
}

list<dtSala*> dtCine::getSalas(){

	return this->salas;
}

