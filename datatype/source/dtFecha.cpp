/*
 * dtFecha.cpp
 *
 *  Created on: Jun 17, 2019
 *      Author: gaston
 */

#include "../header/dtFecha.h"

	dtFecha::dtFecha(){
		this->dia =0;
		this->mes = 0;
		this->anio = 0;
	}

	dtFecha::dtFecha(int d, int m, int a){

		this->dia =d;
		this->mes = m;
		this->anio = a;

	}

	int dtFecha::getDia(){
		return this->dia;
	}
	int dtFecha::getMes(){
		return this->mes;
	}
	int dtFecha::getAnio(){
		return this->anio;
	}


