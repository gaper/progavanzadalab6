#ifndef DATATYPE_HEADER_DTRELOJ_H_
#define DATATYPE_HEADER_DTRELOJ_H_

#include <string>
#include <iostream>
using namespace std;

class dtReloj{
    private:
		int dia;
		int mes;
		int anio;
		int hora;
		int minutos;

	public:
		dtReloj();
		dtReloj(int d, int m, int a, int h, int min);
		virtual ~dtReloj();

		int getDia();
		int getMes();
		int getAnio();
		int getHora();
		int getMinutos();

		friend bool operator < (const dtReloj & f1, const dtReloj & f2);

};

#endif /* DATATYPE_HEADER_DTRELOJ_H_ */
