/*
 * dtCine.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */

#include <string>
#include <iostream>
#include "../header/dtFecha.h"
#include "../header/dtHora.h"
#include "../header/dtDireccion.h"
#include "dtSala.h"
using namespace std;

#ifndef DTCINE_H_
#define DTCINE_H_


class dtCine{

private:

	int nroCine;
	dtDireccion direccion;
	list<dtSala*> salas;

public:
	dtCine();
	dtCine(int nroCine, dtDireccion direccion, list<dtSala*> salas);
	virtual ~dtCine();

	//gettes

	int getNro();
	dtDireccion getDireccion();
	list<dtSala*> getSalas();

};


#endif /* DTCINE_H_ */
