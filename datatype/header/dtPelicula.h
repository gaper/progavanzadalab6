/*
 * dtPelicula.h
 *
 *  Created on: Jun 12, 2019
 *      Author: mp
 */
#include <string>
#include <iostream>
//#include "DtFecha.h"
using namespace std;

#ifndef DTPELICULA_H_
#define DTPELICULA_H_

class dtPelicula{

private:
	string titulo, poster, sinopsis;
	float puntajePromedio;

public:

	dtPelicula();
	dtPelicula(string titulo, string poster, string sinopsis, float puntajePromedio);
	virtual ~dtPelicula();
	//DtConsulta(DtFecha fecha, string motivo);

	//gettes

	string getTitulo();
	string getPoster();
	string getSinopsis();
	float getPromedio();

};

#endif /* DTPELICULA_H_ */
