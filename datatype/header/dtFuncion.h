/*
 * dtFuncion.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */

#include <string>
#include <iostream>
#include "dtFecha.h"
#include "dtHora.h"
#include "dtReloj.h"
using namespace std;

#ifndef DTFUNCION_H_
#define DTFUNCION_H_

class dtFuncion{

private:

	int nroFuncion;
	dtFecha fecha;
	dtHora hora;

public:

	dtFuncion(int nroFuncion, dtFecha fecha, dtHora hora);
	virtual ~dtFuncion();
	//DtConsulta(DtFecha fecha, string motivo);

	//gettes

	int getNro();
	dtFecha getFecha();
	dtHora getHora();
	dtReloj getReloj();

};




#endif /* DTFUNCION_H_ */
