/*
 * DTHORA.h
 *
 */
#include <string>
#include <iostream>
using namespace std;

#ifndef DTHORA_H_
#define DTHORA_H_

class dtHora {

	private:
	int hora;
	int minuto;
	int segundo;

	//getters

	public:
	int getHora();
	int getMinuto();
	int getSegundo();

	dtHora();
	dtHora(int h, int m, int s);


};



#endif /* DTHORA_H_ */
