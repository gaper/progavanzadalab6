/*
 * dtSala.h
 *
 *  Created on: Jun 18, 2019
 *      Author: gaston
 */

#ifndef DATATYPE_HEADER_DTSALA_H_
#define DATATYPE_HEADER_DTSALA_H_
#include <list>

#include "dtFuncion.h"

class dtSala {

private :

	int nroSala;
	int capacidad;
	list<dtFuncion*> funciones;


public:
	dtSala();
	dtSala(int nroSala,int capacidad,list<dtFuncion*> funciones);
	int getCapacidad();
	list<dtFuncion*> getFunciones();
	int getNroSala();

};


#endif /* DATATYPE_HEADER_DTSALA_H_ */
