/*
 * dtDireccion.h
 *
 *  Created on: 12 jun 2019
 *      Author: Usuario
 */
#ifndef DTDIRECCION_H_
#define DTDIRECCION_H_

#include <string>
#include <iostream>
using namespace std;



class dtDireccion {

private:
	string calle;
	int nro;

	//getters

	public:
	dtDireccion();
	dtDireccion(string calle, int nro);
	virtual ~dtDireccion();
	string getCalle();
	int getNro();

};



#endif /* DTDIRECCION_H_ */
