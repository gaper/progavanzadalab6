/*
 * DtFecha.h
 *
 */
#ifndef DTFECHA_H_
#define DTFECHA_H_

#include <string>
#include <iostream>
using namespace std;


class dtFecha {

	private:

	int dia;
	int mes;
	int anio;

	//getters

	public:

	dtFecha();

	dtFecha(int d, int m, int a);

	int getDia();

	int getMes();

	int getAnio();




};



#endif /* DTFECHA_H_ */
