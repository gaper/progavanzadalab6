#ifndef DATATYPE_HEADER_DTUSUARIO_H_
#define DATATYPE_HEADER_DTUSUARIO_H_

#include <string>
using namespace std;

class dtUsuario{
	private:
        string nick;
        string pass;
        string imagen;
        bool esAdmin;
    public:
        dtUsuario();
		dtUsuario(string nick, string pass, string imagen, bool esAdmin);
		virtual ~dtUsuario();

		//getters
		string getNick();
		string getPass();
		string getImagen();
		bool getEsAdmin();

};

#endif /* DATATYPE_HEADER_DTUSUARIO_H_ */
